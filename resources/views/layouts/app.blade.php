<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>





    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
   <!-- Fonty google -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:700' rel='stylesheet' type='text/css'>
    <!-- Styles -->
 {{--  <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.min.css')}}" text="text/css"> --}}      
    


   


{{-- Głowny plik styli less --}}
<link rel="stylesheet" href="{{ URL::asset('assets/source/style.less') }}"  type="text/less">
  <script src="{{ URL::asset('assets/js/less.min.js') }}"></script> 
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
       @stack('header-scripts')
       @stack('styles')

 <style>






        li{
            list-style: none;
        }

    

        #menu-toggle{
            cursor: pointer;
        }



        .nav a{

            text-transform: uppercase;
        }

    

nav .navbar-nav > li > a:hover, .nav .navbar-nav > li > a:focus
        {
            background-color: transparent;
        }

       nav .navbar-nav > li > a:hover, .navbar-nav>.active>a {
        color:#98dafc!important;
        }

        a{
           color: #000;
        }

        a:hover{
            color:#98dafc
        }

        .navbar-default{
            background-color: #fff;
        }

       .navbar-toggle{
        border: none;
        background-color: transparent!important;
       } 

         .navbar-toggle:hover{
           
         /*   color: #5bc0de!important;*/
         }

   @media only screen and (min-width : 992px) 

  {
/*.navbar-nav{
margin-left:180px;
  
}*/

 .navbar-nav:not(.navbar-right); {
  width: 100%;
  float: none;

  text-align: center!important;
  > li {
    float: none;
    display: inline-block;
  }
}
}


@media only screen and (max-width : 991px) 

  {

.navbar-nav li {
      text-align: left;
      margin: 0;
      height: auto;
      border-bottom: 1px solid #e3e3e3;
      }

      .navbar-nav li:first-of-type{
      margin-top: -5px;
      }
 
      .navbar-nav li:last-of-type{
          border-bottom: 0px;
          margin-bottom: -5px;
      }
        .navbar-header {
      float: none;
  }
  .navbar-left,.navbar-right {
      float: none !important;
  }
  .navbar-toggle {
      display: block;
  }
  .navbar-collapse {
      border-top: 1px solid transparent;
      box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
  }
  .navbar-fixed-top {
      top: 0;
      border-width: 0 0 1px;
  }
  .navbar-collapse.collapse {
      display: none!important;
  }
  .navbar-nav {
      float: none!important;
      margin-top: 7.5px;
  }
  .navbar-nav>li {
      float: none;
  }
  .navbar-nav>li>a {
      padding-top: 10px;
      padding-bottom: 10px;
  }
  .collapse.in{
      display:block !important;
  }
  
  }
 
        .social-phone{
      /*   display: inline-block;*/
      padding: 30px 0px;
        }

        .separator{
            margin: 50px 0;
        }

    </style>






    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body id="app-layout">


<div class="container">
    <div class="row">
         <div class="col-md-8 col-md-offset-4">
          
            <div class="social-phone pull-right">
                <a href="https://www.facebook.com/Beata-Nykiel-Photography-1802088870017560/?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a> |
                <a href="tel: 07456 842 843">07456 842 843</a>
                
            </div>
        </div>
        </div>
    
</div>
<nav class="navbar ">
        <div class="container">
            <div class="navbar-header">

        


                 <a  id="menu-toggle"  class="navbar-toggle collapsed"  id="mobile-icon" data-target="#app-navbar-collapse" data-toggle="collapse">
                                    <i class="fa fa-bars"></i>
                 </a>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Laravel
                </a>
            </div>
            s

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li {{ Request::is('/') ? 'class=active' : '' }}><a href="{{ route('home')}}">Home</a></li>
                <li {{ Request::is('galleries') ? 'class=active' : '' }}><a href="{{ route('galleries')}}">Galleries</a></li>
                <li {{ Request::is('sessions') ? 'class=active' : '' }}><a href="{{ route('sessions')}}">Sessions</a></li>
                <li {{ Request::is('testimonals') ? 'class=active' : '' }}><a href="{{ route('testimonals')}}">Testimonals</a></li>
                <li {{ Request::is('about-me') ? 'class=active' : '' }}><a href="{{ route('about')}}">About Me</a></li>
                 <li {{ Request::is('photography-blog') ? 'class=active' : '' }}><a href="{{ route('blog.index')}}">Blog</a></li>
                <li {{ Request::is('contact') ? 'class=active' : '' }}><a href="{{ route('contact')}}">Contact</a></li>
              <span id="separator"></span>
            </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                   @if (Auth::guest())  
                    <li><a href="{{ url('/login') }}">Log in</a></li>
                    @endif
                    @if (!Auth::check())
                    <li><a href="{{ url('/signup') }}">Sign up</a></li>         
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

@if(empty(Auth::user()->photo))
 <img  src="{{url('user/avatar/default.png')}}" width="32px" height="32px">
@else
        <img src="{{url('user/avatar/'. Auth::user()->photo)}}" width="32px" height="32px">

@endif
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                         <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
     
            </div>
        </div>
    </nav>
   

<div class="container">

    @yield('content')
  
    @include('layouts.partials.footer')
</div>  

    <!-- JavaScripts -->
   
    <script src="{{URL::asset('assets/js/bootstrap.min.js')}}"></script>
   

 @stack('scripts')   
</body>
</html>
