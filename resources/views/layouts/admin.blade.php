<!DOCTYPE html>
<html lang="en">
<head>
 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  
    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <!-- Fonty google -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:700' rel='stylesheet' type='text/css'>
    <!-- Styles -->
 {{--  <link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.min.css')}}" text="text/css"> --}}    



{{-- Głowny plik styli less --}}
<link rel="stylesheet" href="{{ URL::asset('assets/source/style.less') }}"  type="text/less">

  <script src="{{ URL::asset('assets/js/less.min.js') }}"></script> 
{{-- @stack('header-scripts') --}}
    <style>
        body {
            font-family: 'Lato';
        }

        .btn{
            border-radius: 0px;
        }

        input{
            border-radius: 0px!important; 
        }

        li{
            list-style: none;
        }

        .fa-btn {
            margin-right: 6px;
        }

        #menu-toggle{
            cursor: pointer;
        }


        .nav a{

            text-transform: uppercase;
        }

    

ul.nav a:hover, .navbar-nav>.active>a {
 color:#98dafc!important;
   background:transparent!important;
}

nav .navbar-nav {
      background:transparent;
}

       .navbar-toggle{
        border: none;
        background-color: transparent!important;
       } 

         .navbar-toggle:hover{
           
         /*   color: #5bc0de!important;*/
         }

  @media only screen and (min-width : 769px) 



   .navbar-nav:not(.navbar-right); {
    width: 100%;
    float: none;
  
    text-align: center!important;
    > li {
      float: none;
      display: inline-block;
    }
  }
}


  @media only screen and (max-width : 768px) 

    {
   .navbar-nav:not(.navbar-right) {
        text-align: left;
        margin: 0;
        height: auto;
        border-bottom: 1px solid #e3e3e3;
        }

        .navbar-nav li:first-of-type{
        margin-top: -5px;
    
   
        .navbar-nav li:last-of-type{
            border-bottom: 0px;
            margin-bottom: -5px;
        }
    
    }

        .social-phone{
      /*   display: inline-block;*/
      padding: 30px 0px;
        }

       .separator{
            margin: 50px 0;
        }

        .footer-container{
            width:1230px;
        }
    </style>

@stack('styles')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
            {{--     <a class="navbar-brand" href="{{ url('/') }}">
        ro            Laravel
                </a> --}}
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                  <li {{ Request::is('admin') ? 'class=active' : '' }}><a href="{{ route('admin.index')}}">Dashboard</a></li>
                <li  {{ Request::is('admin/users') ? 'class=active' : '' }} ><a href="{{ route('admin.users')}}">Users</a></li>
                    <li class="dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Home </a>
                <ul class="dropdown-menu" role="menu">
                    <li {{ Request::is('admin/home') ? 'class=active' : '' }}><a href="{{ route('admin.home.index') }}">Home Content Text</a></li>
                    <li {{ Request::is('admin/home-slideshow') ? 'class=active' : '' }}><a href="{{ route('admin.home-slideshow.index') }}">Home Slideshow</a></li>
                </ul>
                    </li>
                    <li {{ Request::is('admin/gallery') ? 'class=active' : '' }} ><a href="{{ route('admin.gallery.index')}}">Galleries </a></li>
                    <li  {{ Request::is('admin/sessions') ? 'class=active' : '' }} ><a href="{{ route('admin.sessions.index')}}">Sessions </a></li>
                    <li {{ Request::is('admin/about-me') ? 'class=active' : '' }}><a href="{{ route('admin.about-me.index') }}">About Me</a></li>

                    <li {{ Request::is('admin/testimonals') ? 'class=active' : '' }}><a href="{{ route('admin.testimonals.index') }}">Testimonals</a></li>
                    <li {{ Request::is('admin/contact') ? 'class=active' : '' }}><a href="{{ route('admin.contact.index') }}">Contact </a></li>
                           <li {{ Request::is('admin/newsletter') ? 'class=active' : '' }}><a href="{{ route('admin.newsletter.index') }}">Newsletter </a></li>
                      <li {{ Request::is('admin/authors') ? 'class=active' : '' }}><a href="{{ route('admin.authors.index') }}">Registered Authors</a></li>
                      <li {{ Request::is('admin/posts') ? 'class=active' : '' }}><a href="{{ route('admin.posts.index') }}">Posts</a></li>
                       <li {{ Request::is('admin/categories') ? 'class=active' : '' }}><a href="{{ route('admin.categories.index') }}">Categories</a></li>
                        <li {{ Request::is('admin/tags') ? 'class=active' : '' }}><a href="{{ route('admin.tags.index') }}">Tags</a></li>
 <li {{ Request::is('admin/comments') ? 'class=active' : '' }}><a href="{{ route('admin.comments.index') }}">Comments</a></li>


                </ul>
             <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                   @if (Auth::guest())  
                    <li><a href="{{ url('/login') }}">Log in</a></li>
                    @endif
                    @if (!Auth::check())
                    <li><a href="{{ url('/signup') }}">Sign up</a></li>         
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

@if(empty(Auth::user()->photo))
<img  src="{{url('user/avatar/default.png')}}" width="32px" height="32px">
@else


        <img src="{{url('user/avatar/'. Auth::user()->photo)}}" width="32px" height="32px">

@endif

                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                         <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
   
        <div class="container">
        @yield('content')
        @include('layouts.partials.footer')
        </div>
 

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
  <script src="{{URL::asset('assets/js/bootstrap.min.js')}}"></script>
    @stack('scripts')


</body>
</html>
