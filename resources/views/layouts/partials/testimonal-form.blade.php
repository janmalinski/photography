                           

                            <form id="upload_form" class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="/create-testimonal">
                 <div class="col-md-4 col-md-offset-3">
                            <div class="form-group"{{ $errors->has('author') ? ' has-error' : '' }}>
                                <label class="control-label">Your Name</label>
                                <input id="author" type="text" class="form-control" name="author" value="{{ old('author') }}">
                                    @if ($errors->has('author'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('author') }}</strong>
                                    </span>
                                @endif
                            </div>
                        <div class="form-group"{{ $errors->has('email') ? ' has-error' : '' }}>
                            <label class="control-label">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                     @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
               
                        <div class="form-group"{{ $errors->has('testimonal') ? ' has-error' : '' }}>
                            <label class="control-label">Testimonal</label>
                         <textarea id="testimonal" class="form-control" name="testimonal" > 
                         {{ old('testimonal') }}
                         </textarea>
                                   @if ($errors->has('testimonal'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('testimonal') }}</strong>
                                    </span>
                                @endif     
                        </div>
                    </div>
           <div class="col-md-1 col-md-offset-1">
        <div class="form-group">
          <div class="fileinput fileinput-new" data-provides="fileinput" {{ $errors->has('photo') ? ' has-error' : '' }}>
                <div class="fileinput-new thumbnail" style="width: 150px; height: 150px;">
             <?php $photo = ! empty($testimonals->photo) ? $testimonals->photo : 'https://placehold.it/150x150' ?>
             
                        <img  src="https://placehold.it/150x150" alt="Photo">
                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style="width: 150px; height: 150px;;"></div>
                         @if ($errors->has('photo'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('photo') }}</strong>
                                    </span>
                                @endif 
                      <div class="text-center">
                        <span class="btn btn-default btn-file"><span class="fileinput-new">Choose Photo</span><span class="fileinput-exists">Change</span><input id="photo" type="file" name="photo"></span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>

                    </div> 
                  </div>
                </div>
   
        
              <div class="row">
                <div class="col-md-4 col-md-offset-3">
                
                      <button id="click" type="submit" class="btn btn-primary">Save</button>
               
                </div>
              </div>
          
                           <input type="hidden" value="{{ Session::token() }}" name="_token">

                    </form>
                 