       
                  <form id="contact-form" class="form-horizontal" role="form"  enctype="multipart/form-data" method="POST" action="/contact">
                 
                            <div class="form-group"{{ $errors->has('sender') ? ' has-error' : '' }}>
                                <label class="control-label">Your Name</label>
                                <input type="text" class="form-control" name="sender" id="sender" value="{{ old('sender') }}">
                                    @if ($errors->has('sender'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('sender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        <div class="form-group"{{ $errors->has('email') ? ' has-error' : '' }}>
                            <label class="control-label">E-Mail Address</label>
                                <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}">
                                     @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group"{{ $errors->has('phone') ? ' has-error' : '' }}>
                            <label class="control-label">Phone Number(optional)</label>
                                <input type="tel" class="form-control" name="phone" id value="{{ old('phone') }}">
                                     @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                        </div>
                    <div class="form-group"{{ $errors->has('subject') ? ' has-error' : '' }}>
                            <label class="control-label">Subject</label>
                                <input type="text" class="form-control" name="subject" id="subject" value="{{ old('subject') }}">  
                                 @if ($errors->has('subject'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif 
                        </div>
                        <div class="form-group"{{ $errors->has('message') ? ' has-error' : '' }}>
                            <label class="control-label">Message</label>
                         <textarea class="form-control" name="message" id="message"  "> 
                        {{ old('message') }}
                         </textarea>
                                   @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif     
                        </div>
                        <div class="form-group">
                                <button  id="sent" type="submit"   class="btn btn-primary">
                                    Send Message
                                </button>
                        </div>
                               <input type="hidden" value="{{ Session::token() }}" name="_token">
                    </form>
                