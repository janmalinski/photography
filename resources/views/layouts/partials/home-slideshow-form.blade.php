@include('layouts.partials.form-header')

<div class="panel-body">
  <div class="form-horizontal">
    <div class="row">
      <div class="col-md-8">

          @include('layouts.partials.session-error')


        <div class="form-group">
          <label for="title" class="control-label col-md-3">Title</label>
          <div class="col-md-8">
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
          </div>
        </div>

        <div class="form-group">
          <label for="description" class="control-label col-md-3">Description</label>
          <div class="col-md-8">
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
          </div>
        </div>

        <div class="form-group">
          <label for="order" class="control-label col-md-3">Order</label>
          <div class="col-md-8">
            {!! Form::text('order', null, ['class' => 'form-control']) !!}
          
          </div>
        </div>
        
      </div>
      <div class="col-md-4">
        <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail">
              <?php $photo = ! empty($slide->photo) ? $slide->photo : 'placeholder.png' ?>
              {!! Html::image('homeslideshow/thumbs/'.$photo, null, null) !!}
          </div>
          <div class="fileinput-preview fileinput-exists thumbnail"></div>
          <div class="text-center">
            <span class="btn btn-default btn-file"><span class="fileinput-new">Choose Photo</span><span class="fileinput-exists">Change</span>

            {!! Form::file('photo') !!}</span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="panel-footer">
  <div class="row">
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-offset-3 col-md-6">
          <button type="submit" class="btn btn-primary">{{ ! empty($slide->id) ? "Update" : "Save"}}</button>
          <a href="{{ route('admin.home-slideshow.index') }}" class="btn btn-default">Cancel</a>
        </div>
      </div>
    </div>
  </div>
</div>
@push('scripts')
<script src="{{ URL::asset('assets/js/jasny-bootstrap.min.js') }}"></script>
@endpush






