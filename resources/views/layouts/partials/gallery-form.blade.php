@include('layouts.partials.session-error')

<div class="panel-body">
  <div class="form-horizontal">
    
     <div class="form-group">
          <label for="name" class="control-label col-md-3">Name of the gallery</label>
          <div class="col-md-8">
            {!! Form::text('name', null, ['class' => 'form-control', 'id'=>'name']) !!}
          </div>
        </div>
    
           <div class="fileinput fileinput-new" data-provides="fileinput">
       
          <div class="fileinput-preview fileinput-exists thumbnail"></div>
          <div class="text-center">
            <span class="btn btn-default btn-file"><span class="fileinput-new">Choose Photo</span><span class="fileinput-exists">Change</span>{!! Form::file('front_image') !!}</span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
          </div>
        </div>

  </div>
</div>
<div class="panel-footer">
  
      <div class="row">
        <div class="col-md-offset-3 col-md-6">
          <button type="submit" class="btn btn-primary">{{ ! empty($slide->id) ? "Update" : "Save"}}</button>
          <a href="{{ route('admin.gallery.index') }}" class="btn btn-default">Cancel</a>
        </div>
      </div>
    
</div>
