<div class="panel-body">
  <div class="form-horizontal">

    <div class="row">
      <div class="col-md-8">
        <div class="form-group">
          <label for="title" class="control-label col-md-3">Title</label>
          <div class="col-md-8">
              @include('layouts.partials.session-error')
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
          </div>
        </div>
        <div class="form-group">
          <label for="description" class="control-label col-md-3">Description</label>
          <div class="col-md-8">
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
          </div>
        </div>
            <div class="form-group">
          <label for="url" class="control-label col-md-3">Url Video</label>
          <div class="col-md-8">
            {!! Form::text('url', null, ['class' => 'form-control']) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="panel-footer">
  <div class="row">
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-offset-3 col-md-6">
          <button type="submit" class="btn btn-primary">{{ ! empty($content->id) ? "Update" : "Save"}}</button>
          <a href="{{ route('admin.sessions.index') }}" class="btn btn-default">Cancel</a>
        </div>
      </div>
    </div>
  </div>
</div>
