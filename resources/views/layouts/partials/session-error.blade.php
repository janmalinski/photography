{{-- @if(Session::has('flash_error'))

    <div class="alert alert-danger">
        {{ Session::get('flash_error') }}
    </div>

@endif --}}

<div class="text-center">

@if(Session::has('flash_message'))

    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>

@endif

@if(count($errors) > 0)

    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

@endif


</div>





