  <div class="clearfix"></div>
 <li>


<div class="media">
  <div class="media-left">
    <a href="#">
      @if(empty($comment->owner->photo))
  <img  src="user/avatar/default.png" width="32px" height="32px">
@else
        <img src="{{url('user/avatar/'.$comment->owner->photo)}}" width="32px" height="32px">

@endif
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">{{ $comment->owner->name }} <small>said {{ $comment->created_at->diffForHumans() }}</small></h4>
      <article>{{ $comment->body }}</article>
  </div>
</div>





@include('layouts.partials.comment.comment-form',['parentId'=> $comment->id])

{{-- if this comment has replies oh its own? --}}
        @if(isset($comments[$comment->id]))

			@include('layouts.partials.comment.comments-list',['collection'=>$comments[$comment->id]])

        @endif
</li>
  <div class="clearfix"></div>



