﻿     

  <div class="panel-body">
    <div class="form-horizontal">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @include('layouts.partials.session-error')

          <div class="form-group">
            <label for="introduction_title" class="control-label col-md-3">Introduction Title</label>
            <div class="col-md-8">
              {!! Form::text('introduction_title', null, ['class' => 'form-control']) !!}
            </div>
          </div>

          <div class="form-group">
            <label for="introduction" class="control-label col-md-3">Introduction</label>
            <div class="col-md-8">
              {!! Form::textarea('introduction', null, ['class' => 'form-control']) !!}
            </div>
          </div>

          <div class="form-group">
            <label for="advantages_title" class="control-label col-md-3">Advantages Title</label>
            <div class="col-md-8">
              {!! Form::text('advantages_title', null, ['class' => 'form-control']) !!}
            </div>
          </div>

           <div class="form-group">
            <label for="advantages" class="control-label col-md-3">Advantages</label>
            <div class="col-md-8">
              {!! Form::textarea('advantages', null, ['class' => 'form-control']) !!}
            </div>
             </div>

             <div class="form-group">
            <label for="how_i_work_title" class="control-label col-md-3">How I work Title</label>
            <div class="col-md-8">
              {!! Form::text('how_i_work_title', null, ['class' => 'form-control']) !!}
            </div>
             </div>


             <div class="form-group">
            <label for="how_i_work" class="control-label col-md-3">How I work</label>
            <div class="col-md-8">
              {!! Form::textarea('how_i_work', null, ['class' => 'form-control']) !!}
            </div>
          </div>
      </div>
    </div>
  </div>
 </div>   
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-8">
        <div class="row">
          <div class="col-md-offset-3 col-md-6">
            <button type="submit" class="btn btn-primary">{{ ! empty($content->id) ? "Update" : "Save"}}</button>
            <a href="{{ route('admin.home.index') }}" class="btn btn-default">Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </div>


 





