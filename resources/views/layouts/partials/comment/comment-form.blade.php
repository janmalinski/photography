 @if(!Auth::check()) 
                  
                        @else
       
        {!! Form::open(['id'=>'comment-form', 'method'=>'POST', 'route' => 'createComment']) !!}

@if(isset($parentId))

	<input name="parent_id"  type="hidden" value="{{ $parentId }}">

@endif
    @if(isset($parentId))
        <div class="comment-reply-container">
  
        <a  class="toggle-reply">Reply</a>

        <div class="comment-reply col-sm-6">
    @endif
    @if(!isset($parentId))
        <h4>Write Comment</h4>
 @endif
            <input type="hidden" name="post_id" value="{{ $post->id }}">
                            <div class="form-group">
                                {{Form::label('body', 'Comment:') }}
                                   @if(isset($parentId))
                                {{ Form::textarea('body',null, ['id'=>'body', 'class'=>'form-control', 'rows'=>3]) }}
                                  @endif
                                   @if(!isset($parentId))
                                   {{ Form::textarea('body',null, ['id'=>'body', 'class'=>'form-control', 'rows'=>5]) }}
                                   @endif
                            </div>
             @if(isset($parentId))
                {{ Form::submit('Reply', ['class'=> 'btn btn-primary reply-save-button']) }}
                
             @endif
             @if(!isset($parentId))
            {{ Form::submit('Submit Comment', [ 'id'=>'comment-save-button', 'class'=> 'btn btn-primary']) }}
     
             @endif         
        {!! Form::close() !!}
        @if(isset($parentId))
                    </div>              
         </div>
        @endif
 @endif






             