@extends('layouts.admin')

@section('content')
<section class="edit-contact">
       <div class="panel panel-default">
            <div class="panel-heading">
              <strong>Edit Contact</strong>
            </div>  

						
				 {!! Form::model($slide,[ 'files'=>true, 'method'=>'PUT','action'=>['HomeSlideshowController@update',$slide->id]])  !!}
        

{!! Form::hidden('id', $slide->id) !!}
           
      @include('layouts.partials.home-slideshow-form')

      {!! Form::close() !!}
          </div>
</section>
@endsection
