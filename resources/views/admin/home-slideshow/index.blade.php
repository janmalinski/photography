@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Home Slideshow
@endsection
@section('content')

<style>
  .pager li>a, .pager li>span{
    border-radius: 0px;
  }

  img{
    padding: 40px 5px 0px 0px;
  }

   .separator{
            margin: 50px 0;
        }

</style>
 <section class="home-slideshows">

	     <div class="panel panel-default">


      <div class="panel-heading clearfix">

        <div class="pull-left">
          <h4>All Slideshows</h4>
        </div>

        <div class="pull-right">

            <a href="{{ route('admin.home-slideshow.create') }}" class="btn btn-success">
              <i class="glyphicon glyphicon-plus"></i> 
              Add Slideshow
            </a>
        </div>
      </div>
 <div class="separator"></div>
<div class="row">
      <div class="col-md-8 col-md-offset-2">
        @include('layouts.partials.session-error')

      </div>
</div>

           <div class="row">

              <div class="col-xs-12">
  

@foreach($slides as $slide)
            
     <?php  $photo = $slide->photo  ?>           

 {!! Html::image('homeslideshow/thumbs/'.$photo, $slide->title, ['class'=>'img-responsive center-block', null, null]) !!}

                   <div class="text-center">
                  
                      <h3>{{  $slide->title }} </h3> 
                       <p>{{  $slide->description }}</p>
                
                    <div class="links">   

                      <a href="{{ route('admin.home-slideshow.edit', ['id'=>$slide->id]) }}"  class="text-info"  title="Edit">
                          Edit
                     </a>
                    |
                    <a href="{{ route('admin.home-slideshow.delete', ['id'=>$slide->id]) }}" class="text-danger">Delete</a>
                        
                    </div>

                    </div>
           
@endforeach




                   

          </div>
      </div>
                      
          </div>

          <div class="text-center">

    
            @if($slides->currentPage() !== 1)

              <a href="{{ $slides->previousPageUrl() }}"><span class="fa fa-caret-left"></span></a>

            @endif

            @if($slides->currentPage() !== $slides->lastPage()  && $slides->hasPages())

              <a href="{{ $slides->nextPageUrl() }}"><span class="fa fa-caret-right"></span></a>

            @endif

          
          </div>

</section>
@endsection