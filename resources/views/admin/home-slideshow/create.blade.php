@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Create Home Slideshow
@endsection

@section('content')

	<section class="create-homeslideshow">
		
			<div class="row">
				<div class="col-xs-12">
	       			<div class="panel panel-default">
			            <div class="panel-heading">
			              <strong>Add Slide</strong>
			            </div>  

						{!! Form::open(['route' => 'admin.home-slideshow.store', 'files'=> true]) !!}
						
						@include('layouts.partials.home-slideshow-form')

			            {!! Form::close() !!}
	            	</div>
	        	</div>
	    	</div>

	</section>

@endsection