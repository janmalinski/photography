
@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - List of Newsletter Subscribers
@endsection
@section('content')
	
	<section class="subscribers-list">
	
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
				@include('layouts.partials.session-error')
				<h2>List of Subscribers</h2>
@foreach($subscribers  as $subscriber)
				
					<p>{{ $subscriber->name }}</p> 
			
				
					<p>{{ $subscriber->email }}</p>
						<div class="clearfix"></div>
					<div class="pull-right">
					 <a href="{{ route('admin.newsletter.delete', ['id'=>$subscriber->id] ) }}" class="text-danger">Delete</a>
					 </div>
					<hr>


@endforeach
   <div class="text-center">

    
            @if($subscribers->currentPage() !== 1)

              <a href="{{ $subscribers->previousPageUrl() }}"><span class="fa fa-caret-left"></span></a>

            @endif

            @if($subscribers->currentPage() !== $subscribers->lastPage()  && $subscribers->hasPages())

              <a href="{{ $subscribers->nextPageUrl() }}"><span class="fa fa-caret-right"></span></a>

            @endif

          
          </div>

				</div>
					

			</div>
	
	</section>


@endsection