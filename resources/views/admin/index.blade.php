@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Admin Dasboard
@endsection
<style>
	
section{
  padding: 10px 0px;
}

section:last-of-type{
	  padding-bottom: 30px;
}
</style>


@section('content')

<section class="dashboard">

	
	<section class="contact-messages">

		@if(count($contact_messages) == 0)
				<h3 class="text-center">No Messages</h3>

			@endif

			@if(count($contact_messages) > 0)
				<h3 class="text-center">Contact Messages</h3>

			@endif
			<hr>
				@include('layouts.partials.session-error')
	
	@foreach($contact_messages as $contact_message)
	<div class="row">
		<div class="col-xs-12">
		 	<section class="contact-messages text-center">

		

			
			<h3> {{ $contact_message->subject }}</h3>
				<h4>{{ $contact_message->sender->name }} | {{ $contact_message->sender->email }}</h4>
				 on {{ $contact_message->created_at }}
				 <a data-toggle="modal" data-target="#myModal">View</a> 




<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><strong> {{ $contact_message->subject }}</strong></h4>
      </div>
      <div class="modal-body">
        <blockquote class="quote">
			                	
			                    <p>
			                        {{ $contact_message->message }}
			                 </p>

								
								<strong>{{ $contact_message->sender->email }}</strong>

			                     <footer><cite>{{ $contact_message->sender->name }} on {{ $contact_message->created_at }}</cite></footer>
			                </blockquote>   
      </div>
     
			<div class="modal-footer">
			                    <a href="{{ route('admin.contact-message.delete', ['id'=>$contact_message->id] ) }}" class="text-danger">Delete</a>
			</div>
 		</div>
	</div>
</div>
			</section>
		</div>
	</div>

  @endforeach
  		
		
  				<div class="text-center">
				   @if($contact_messages->currentPage() !== 1)

				              <a href="{{ $contact_messages->previousPageUrl() }}"><span class="fa fa-caret-left"></span></a>

				            @endif

				            @if($contact_messages->currentPage() !== $contact_messages->lastPage()  && $contact_messages->hasPages())

				              <a href="{{ $contact_messages->nextPageUrl() }}"><span class="fa fa-caret-right"></span></a>

				            @endif
				</div>
			</section>
</section>
@endsection
