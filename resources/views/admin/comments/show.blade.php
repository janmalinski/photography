@extends('layouts.admin')


@section('content')
 
	@if(count($comments) > 0)

	<h1>Comments</h1>

<table class="table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Body</th>
			<th>View</th>
			<th>Approve/Unapprove</th>
			<th>Delete</th>

		</tr>
	</thead>
	<tbody>
		@foreach($comments as $comment)
		<tr>
			<td>{{ $comment->owner->name }}</td>
			<td>{{ $comment->owner->email }}</td>
			<td>{{ $comment->body }}</td>
			<td><a href="{{ route('blog.single',$comment->post->slug) }}">View Post</a></td>
		
			<td>



				@if($comment->is_active == 1)

				{!! Form::open(['method'=>'PATCH','action'=>['PostCommentsAdminController@update',$comment->id]]) !!}
				
				<input type="hidden" name="is_active" value="0">
				
				<div class="form-group">
					{!! Form::submit('Unapprove', ['class'=> 'btn btn-primary']) !!}
				</div>

				{!! Form::close() !!}

				

				@else

	{!! Form::open(['method'=>'PATCH','action'=>['PostCommentsAdminController@update',$comment->id]]) !!}
				
				<input type="hidden" name="is_active" value="1">
				
				<div class="form-group">
					{!! Form::submit('Approve', ['class'=> 'btn btn-primary']) !!}
				</div>

				{!! Form::close() !!}

				@endif


			</td>

			<td>				

	{!! Form::open(['method'=>'DELETE','action'=>['PostCommentsAdminController@destroy',$comment->id]]) !!}
				
				<input type="hidden" name="is_active" value="1">
				
				<div class="form-group">
					{!! Form::submit('Delete', ['class'=> 'btn btn-danger']) !!}
				</div>

				{!! Form::close() !!}</td>

		</tr>
		@endforeach

	</tbody>
</table>


@else

<h1 class="text-center">No Comments</h1>

	@endif

@endsection