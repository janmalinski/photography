@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - List of Galleries
@endsection
@section('content')
<style>
  .pager li>a, .pager li>span{
    border-radius: 0px;
  }

  .edit-delete{
    margin-right: 10px;
    margin-bottom: 10px;
  }

  section{
    padding: 30px;
  }

.separator{
            margin: 50px 0;
        }

</style>
 <section class="contact-content">
	<div class="panel panel-default">
      <div class="panel-heading clearfix">

        <div class="pull-left">
          <h4>Contact Content</h4>
        </div>
       @if(count($contact_content) == 0)
        <div class="pull-right">      
            <a href="{{ route('admin.contact.create') }}" class="btn btn-success">
              <i class="glyphicon glyphicon-plus"></i> 
              Add Content of Contact Page
            </a>
        </div>
        @endif
      </div>
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('layouts.partials.session-error')
        </div>
      </div>
      <div class="separator"></div>
           <div class="row">
              <div class="col-xs-12">
                  <div class="card">
                      <ul>
                        @if(count($contact_content) == 0)
                        <li class="text-danger">No Contact Content</li>
                        @endif
                        @foreach($contact_content as $content)
                  
    <?php $photo = ! empty($content->photo) ? $content->photo : 'placeholder.png' ?>
                        

              {!! Html::image('contactcontent/thumbs/'.$photo,$content->title, ['class'=>'img-responsive center-block', null, null]) !!}

                        <li >
                          <article>
                            <div class="message-info">
                            <h3>Introdcuction Title:</h3>
                            <h4>{{ $content->title }}</h4>
                             <h3>Description:</h3>
                            <h4>{{ $content->description}}</h4>
                          </article>
                        </li>
                     <div class="edit-delete pull-right">
                 
                      <a href="{{ route('admin.contact.edit', ['id'=>$content->id]) }}"  class="text-info"  title="Edit">
                          Edit
                     </a>
                    |
                    <a href="{{ route('admin.contact.delete', ['id'=>$content->id] ) }}" class="text-danger">Delete</a>
                    </div>
                        @endforeach
                      </ul>
                  </div>
          	</div>
      	</div>                  
	</div>
  </div>
  </section>

				
@endsection