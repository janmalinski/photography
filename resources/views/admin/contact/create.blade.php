@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Create Contact Content
@endsection
@section('content')

 <div class="panel panel-default">
            <div class="panel-heading">
              <strong>Add Contact Text Content</strong>
            </div>  

			{!! Form::open(['route' => 'admin.contact.store', 'files'=> true]) !!}
			
			@include('layouts.partials.contact-content-form')

            {!! Form::close() !!}
 </div>

@endsection