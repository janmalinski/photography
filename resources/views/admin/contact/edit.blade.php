@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Create Home Content
@endsection
@section('content')

     <div class="panel panel-default">
            <div class="panel-heading">
              <strong>Add Home Text Content</strong>
            </div>  

				{!! Form::model($content,['files'=>true,'route' => ['admin.contact.update', $content->id], 'method'=> 'PUT']) !!}
          
			
			@include('layouts.partials.contact-content-form')

            {!! Form::close() !!}
          </div>

@endsection