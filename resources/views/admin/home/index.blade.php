@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Home Content
@endsection
@section('content')

<style>
  .pager li>a, .pager li>span{
    border-radius: 0px;
  }

  .edit-delete{
    margin-right: 10px;
    margin-bottom: 10px;
  }



</style>
 
	     <div class="panel panel-default">


      <div class="panel-heading clearfix">

        <div class="pull-left">
          <h4>Home Content Text</h4>
        </div>
       @if(count($home_content) == 0)
        <div class="pull-right">      
            <a href="{{ route('admin.home.create') }}" class="btn btn-success">
              <i class="glyphicon glyphicon-plus"></i> 
              Add Text Content of Home Page
            </a>
        </div>
        @endif
      </div>
 

        @include('layouts.partials.session-error')



           <div class="row">

              <div class="col-xs-12">

                  <div class="card">
                      <ul>
                        @if(count($home_content) == 0)
                        <li >No Home Content</li>
                        @endif
                        @foreach($home_content as $content)   
                        <li >
                          <article>
                            <div class="message-info">
                            <h3>Introdcuction Title:</h3>
                            <h4>{{ $content->introduction_title }}</h4>
                             <h3>Introdcuction:</h3>
                            <h4>{{ $content->introduction}}</h4>
                            <hr>  
                             <h3>Advantages Title:</h3>
                            <h4>{{ $content->advantages_title }}</h4>
                             <h3>Advantages:</h3>
                            <h4>{{ $content->advantages }}</h4>
                            <hr>
                                <h3>How I work Title:</h3>
                            <h4>{{ $content->how_i_work_title }}</h4>
                                  <h3>How I work:</h3>
                            <h4>{{ $content->how_i_work }}</h4>
                            </div>
                          </article>
                        </li>
                     <div class="edit-delete pull-right">
                      <a href="{{ route('admin.home.edit', ['id'=>$content->id]) }}"  class="text-info"  title="Edit">
                          Edit
                     </a>
                    <a href="{{ route('admin.home.delete', ['id'=>$content->id] ) }}" class="text-danger">Delete</a>
                    </div>
                        @endforeach
                      </ul>
                  </div>
           </div>
        </div>            
    </div>
@endsection