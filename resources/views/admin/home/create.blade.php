@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Create Home Content
@endsection
@section('content')

     <div class="panel panel-default">
            <div class="panel-heading">
              <strong>Add Home Text Content</strong>
            </div>  

			{!! Form::open(['route' => 'admin.home.store', 'files'=> false]) !!}


			{!! Form::model($slides,['files'=>true,'route' => ['admin.home-slideshow.store', $slide->id], 'method'=> 'POST']) !!}
			
			@include('layouts.partials.home-text-form')

            {!! Form::close() !!}
     </div>

@endsection