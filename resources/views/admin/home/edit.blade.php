@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Create Home Content
@endsection
@section('content')

     <div class="panel panel-default">
            <div class="panel-heading">
              <strong>Add Home Text Content</strong>
            </div>  

				{!! Form::model($content,['route' => ['admin.home.update', $content->id], 'method'=> 'PATCH']) !!}
          
			
			@include('layouts.partials.home-text-form')

            {!! Form::close() !!}
          </div>

@endsection