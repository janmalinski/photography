@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Testimonals
@endsection


@section('content')
<style>
	h2{
		margin-bottom: 20px;
	}
	.testimonal{
		padding: 30px;
	}
	cite{
		margin-top:5px!important; 
	}
	.media-body{
		    vertical-align: middle;
	}
</style>



		
@if(count($testimonals)>0)

<section class="testimonals">
	<h2 class="text-center">Latest Testimonals</h2>

@endif

<div class="row">
	<div class="col-md-8 col-md-offset-2">



			@for($i = 0 ; $i <count($testimonals); $i++)

		<article class=testimonal>

					<blockquote>
				<p>{{ $testimonals[$i]->testimonal }}</p>
			
								
		
				<div class="media">
				  <div class="media-left media media-middle">
				
				<img class="img-circle media-object" src="{{ url('testimonal/avatar/' . $testimonals[$i]->author->photo) }}" width="64px" height="64px">
				  
				  </div>
				  <div class="media-body">
				 
								 <cite  ">{{ $testimonals[$i]->author->name  }}</cite> 
						
				  
				  </div>
				  		     <div class="links pull-right">   

                    
                    
                    <a href="{{ route('admin.testimonal.delete', ['id'=>$testimonals[$i]->id]) }}" class="text-danger">Delete</a>
                        
                    </div>

				</div>
			
			</blockquote>
			</article>
		
			@endfor
		</div>
	</div>

		
			  <div class="text-center">

    
            @if($testimonals->currentPage() !== 1)

              <a href="{{ $testimonals->previousPageUrl() }}"><span class="fa fa-caret-left"></span></a>

            @endif

            @if($testimonals->currentPage() !== $testimonals->lastPage()  && $testimonals->hasPages())

              <a href="{{ $testimonals->nextPageUrl() }}"><span class="fa fa-caret-right"></span></a>

            @endif

          
          </div>
 
	</section>


@endsection