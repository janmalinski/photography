@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Posts Content
@endsection


@section('content')
<section class="posts-list">
	<div class="row">
		<div class="col-md-10">
		   @include('layouts.partials.session-error')
			<h1>All Posts</h1>
		</div>
		<div class="col-md-2">
			<a href="{{ route('admin.posts.create') }}" class="btn btn-lg btn-block btn-primary">Create New Post</a>
		</div>

	</div>
			<hr>
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Title</th>
					<th>Body</th>
					<th>Created at</th>
					<th>Front Image</th>
					<th>Number of Images</th>
					<th>Number of  Comments</th>
					<th></th>
					<th></th>
				</thead>
				<tbody>
						@foreach($posts as $post)
							<tr>
							<th>{{ $post->id }}</th>
							<td>{{ $post->title }}</td>
							<td>{{ substr(strip_tags($post->body), 0, 50) }}{{ strlen(strip_tags($post->body)) > 50 ? '...': '' }}</td>
							<td>{{ date('M j, Y', strtotime($post->created_at)) }}</td>
							<td>@if (count($post->image) === 1)
									Yes
							     @else
									No
								@endif
							 </td>
							<td>{{ $post->post_images->count() }}</td>
							<td>	
							@if( $post->comments()->count() > 0)
							{{   	$post->comments()->count()}}
							@else
							0
							 @endif 
							</td>
							   <td>
							    	<a href="{{ route('admin.posts.edit', $post->id) }}" class="btn btn-default">Edit / Add Front Image</a>
							    </td>
								<td>
								<a href="{{ route('admin.posts.show', $post->id) }}" class="btn btn-default">View / Add Images / Delete</a> 
							    </td>
							 
							    <td>
							    	<a href="{{ route('admin.comments.show', $post->id) }}" class="btn btn-default">View Comments</a>
							    </td>
							</tr>
						@endforeach
				</tbody>
			</table>
			     <div class="text-center">

    					{{ $posts->links() }}
         {{--    @if($posts->currentPage() !== 1)

              <a href="{{ $posts->previousPageUrl() }}"><span class="fa fa-caret-left"></span></a>

            @endif

            @if($posts->currentPage() !== $posts->lastPage()  && $posts->hasPages())

              <a href="{{ $posts->nextPageUrl() }}"><span class="fa fa-caret-right"></span></a>

            @endif --}}

          
          </div>
		</div>
	</div>
	</section>
@endsection