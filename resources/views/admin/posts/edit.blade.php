@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Edit {{ $post->title}}
@endsection
@push('styles')
<link rel="stylesheet" href="{{ URL::asset('assets/css/select2.min.css') }}"  type="text/css">
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  tinymce.init({
    selector:'textarea',
    plugins: 'link code',
    menubar: false
  });
</script>
@endpush
@section('content')

 

<div class="row">
{{-- model form binding --}}
{!! Form::model($post, ['route'=> ['admin.posts.update', $post->id], 'method'=>'PUT', 'files'=>true]) !!}
<div class="col-md-8">

{{ Form::label('title', 'Title:') }}
{{ Form::text('title', null, ['class'=>'form-control input-lg']) }}
{{ Form::label('slug', 'Slug:') }}
{{ Form::text('slug', null, ['class'=>'form-control input-lg']) }}
{{ Form::label('category_id', 'Category:') }}
{{-- $cats = ['key'=>'value'] tu $cats = ['id' = 'name'] co9takiego.. --}}
{{-- null zostanie zamienione przez model form binding na post->category_id --}}
{{ Form::select('category_id', $cats, null, ['class' => 'form-control']) }}

{{ Form::label('tags', 'Tags:') }}
{{ Form::select('tags[]', $tags2, null, ['class'=>'form-control select2-multi', 'multiple'=>'multiple']) }}

<img class="img-responsive post-image" src="{{ asset('post/frontimage/'.$post->image) }}" alt="{{ $post->title }}" width="555">

{{ Form::label('image', 'Update Front Image:') }}
{{ Form::file('image') }}

{{ Form::label('body', 'Body:') }}

{{ Form::textarea('body', null, ['class'=>'form-control']) }}
 </div>
<div class="col-md-4">
  <div class="well">
      <dl class="dl-horizontal">
      <dt>Created At:</dt>
      <dd>{{ date('M j, Y h:i a', strtotime($post->created_at)) }}</dd>
      </dl>
        <dl class="dl-horizontal">
      <dt>Last Updated:</dt>
      <dd>{{ date('M j, Y h:i a', strtotime($post->updated_at)) }}</dd>
      </dl>
      <hr>
      <div class="row">
        <div class="col-sm-6">
          <a href="{{ route('admin.posts.show', ['id'=>$post->id]) }}" class="btn btn-danger btn-block">Cancel</a>
        </div>
        <div class="col-sm-6">
         {{ Form::submit('Save Changes', ['class'=> 'btn btn-primary btn-block']) }}
        </div>
      </div>

  </div>
</div>
{!! Form::close() !!}
</div>
@endsection
@push('scripts')
  <script src="{{ URL::asset('assets/js/select2.min.js') }}" ></script>
  <script>
    $('.select2-multi').select2();
// set the chosen values
// getRelatedIds() - laravel helper which igive array of ids from example. tags table
// json_encode by jquery/javascript czytała php array
    $('.select2-multi').select2().val({!! json_encode($post->tags()->getRelatedIds()) !!}).trigger('change');
  </script>
@endpush