@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Create New Post
@endsection
@push('styles')
<link rel="stylesheet" href="{{ URL::asset('assets/css/select2.min.css') }}"  type="text/css">
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  tinymce.init({
    selector:'textarea',
    plugins: 'link code',
    menubar: false
  });
</script>
@endpush

@section('content')


<div class="panel-body">
  <div class="form-horizontal">

    <div class="row">
      <div class="col-md-8">

      {!! Form::open(['route' => 'admin.posts.store', 'files'=>'true']) !!}

        <div class="form-group">
          <label for="title" class="control-label col-md-3">Title</label>
          <div class="col-md-8">
              @include('layouts.partials.session-error')
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
          </div>
        </div>

        <div class="form-group">
          <label for="slug" class="control-label col-md-3">Slug</label>
          <div class="col-md-8">
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
          </div>
        </div>
           <div class="form-group">
          <label for="category_id" class="control-label col-md-3">Category</label>
          <div class="col-md-8">
            <select class="form-control" name="category_id">
              @foreach($categories as $category)
              <option value="{{ $category->id }}">{{ $category->name }}</option>
              @endforeach
            </select>
          </div>
        </div>

          <div class="form-group">
          <label for="tags" class="control-label col-md-3">Tag</label>
          <div class="col-md-8">
            <select class="form-control select2-multi " name="tags[]" multiple="multiple">
              @foreach($tags as $tag)
              <option value="{{ $tag->id }}">{{ $tag->name }}</option>
              @endforeach
            </select>
          </div>
        </div>
          <div class="form-group">
         
          <label for="featured_image" class="control-label col-md-3">Upload Front Image:</label>
          <div class="col-md-8">
 
        {{ Form::file('image', null, ['class'=>'form-control']) }}
      </div>
          </div>
          </div>
        <div class="form-group">

         
          <div class="col-md-8 col-md-offset-2">
            <label for="body" class="control-label">Body:</label>
            {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
          </div>

         
    @if(Auth::user()->hasRole('Admin'))
       <input type="hidden" value="{{$user =  Auth::user()->id }}" name="user_id"> 
    @endif





        </div>
      </div>
    </div>
  </div>

<div class="panel-footer">
  <div class="row">
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-offset-3 col-md-6">
          <button type="submit" class="btn btn-primary">{{ ! empty($post->id) ? "Update" : "Save"}}</button>
          <a href="{{ route('admin.posts.index') }}" class="btn btn-default">Cancel</a>
        </div>
      </div>
    </div>
  </div>
</div>
            {!! Form::close() !!}
@endsection
@push('scripts')
  <script src="{{ URL::asset('assets/js/select2.min.js') }}" ></script>
  <script>
    $('.select2-multi').select2();
  </script>
@endpush