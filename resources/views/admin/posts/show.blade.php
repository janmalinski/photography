@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - {{ $post->title }}
@endsection
@push('styles')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
  <link rel="stylesheet" href="{{ URL::asset('assets/css/post-image.css') }}" type="text/css"/>
  <link rel="stylesheet" href="{{ URL::asset('assets/css/lightbox.css') }}" type="text/css"/>
@endpush
@section('content')
<section class="backend-posts">
<div class="row">
<div class="col-md-8">
            @include('layouts.partials.session-error')
  @if(count($post->image)> 0)
            <img src="{{ asset('post/frontimage/'.$post->image) }}" alt="{{ $post->title }}"/>
  @endif
  <h1>{{ $post->title }}</h1>
   {{-- {!! some content - only when you sure when is it secure  !!} --}}
  <p class="lead">{!! $post->body !!}</p>
  <hr>
      <div class="tags">
   <h3>Tags: <small>{{ $post->tags()->count() }} total </small> </h3>
      @foreach($post->tags as $tag)
        <span class="label label-default">{{ $tag->name }}</span>
      @endforeach
      </div>
      <div id="backend-comments">
        <h3>Comments: <small>{{ $post->comments()->count() }} total </small> </h3>
@if( $post->comments()->count() > 0)
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Comment</th>
              <th>Approve/Unapprove</th>
            </tr>
          </thead>

          <tbody>
            @foreach($post->comments as $comment)
            <tr>
              <td>{{ $comment->owner->name }}</td>
              <td>{{ $comment->owner->email }}</td>
              <td>
              {{ $comment->body }}
              </td>
              <td>
             <a href="{{ route('admin.comments.show', $post->id) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
             {{--    <a href="{{ route('comments.edit', $comment->id) }}" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a> --}}
               {{-- <a href="{{ route('comments.delete', $comment->id) }}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a> --}}
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
@endif

      </div>
    </div>
 <div class="col-md-4">
  <div class="well">
     <dl class="dl-horizontal">
      <label>Url:</label>
      <p><a href="{{ url('blog/'.$post->slug) }}">{{ $post->slug}}</a></p>
      </dl>
        <dl class="dl-horizontal">
      <label>Category:</label>
      <p>{{ $post->category->name }}</p>
      </dl>
      <dl class="dl-horizontal">
      <label>Created At:</label>
      <p>{{ date('M j, Y h:i a', strtotime($post->created_at)) }}</p>
      </dl>
        <dl class="dl-horizontal">
      <label>Last Updated:</label>
      <p>{{ date('M j, Y h:i a', strtotime($post->updated_at)) }}</p>
      </dl>
      <hr>
      <div class="row">
        <div class="col-sm-6">
          <a href="{{ route('admin.posts.edit', ['id'=>$post->id]) }}" class="btn btn-primary btn-block">Edit / Add Front Image</a>
        </div>
        <div class="col-sm-6">
          <a href="{{ route('admin.posts.delete', ['id'=>$post->id]) }}" class="btn btn-danger btn-block">Delete</a>
        </div>
         <div class="col-sm-12">
          <a href="{{ route('admin.posts.index') }}" class="btn btn-default btn-block"><< See All Posts</a>
        </div>
      </div>
  </div>
</div>
    </div>
  <div class="row">
    <div class="col-md-8">
         <div id="post-images">
           @foreach ($post->post_images as $image)
         <li>
            <a href="{{ url($image->file_path) }}" data-lightbox="mygallery">
            <img class="img-responsive center-block"  src="{{ url('post/thumb/' .$image->file_name) }}" >
            </a>
          </li>
          @endforeach
            
      </div>
   
    </div>
</div>

  <div class="row">
    <div class="col-md-8">
       <h3>Add Images</h3>
      <form action="{{ url('admin/post-image/do-upload') }}" 
      class="dropzone"
       id="addPostImages">

       {{ csrf_field() }}

       <input type="hidden" name="post_id" value="{{ $post->id }}">  
      </form>
    </div>
   </div>
  </section>
@endsection
@push('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js" ></script>
  <script  src="{{  URL:: asset('assets/js/lightbox.js') }}"></script>
  <script  src="{{  URL:: asset('assets/js/post-image.js') }}"></script>
@endpush