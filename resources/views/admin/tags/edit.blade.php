@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Edit {{ $tag->name }} Tag
@endsection

@section('content')

<div class="row">
	<div class="col-md-8">



	{{ Form::model($tag, ['route'=>['admin.tags.update', $tag->id], 'method'=>'PUT']) }}
		{{ Form::label('name', 'Name:')}}
		{{ Form::text('name', null, ['class'=>'form-control']) }}
		

		{{ Form::submit('Save Changes',['class'=>'btn btn-primary']) }}
		
	{{ Form::close() }}
	</div>
</div>

@endsection
