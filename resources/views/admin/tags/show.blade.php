@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - {{ $tag->name }}
@endsection

@section('content')
	<div class="row">
		<div class="col-md-8">
		@include('layouts.partials.session-error')
			<h1>{{ $tag->name }} Tag &nbsp<small class="text-info">

@if (count($tag->posts) === 1)
  
   {{ $tag->posts()->count() }} Post

@elseif (count($tag->posts) > 1)
   	{{ $tag->posts()->count() }} Posts
@else
    I don't have any Posts!
@endif


		
		
			 </small></h1>
		</div>
		<div class="col-md-2">
			<a href="{{ route('admin.tags.edit', $tag->id) }}" class="btn btn-primary pull-right btn-block">Edit</a>
		</div>
			<div class="col-md-2">
			<a href="{{ route('admin.tags.delete', $tag->id) }}" class="btn btn-danger pull-right btn-block">Delete</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Title</th>
					<th>Tags</th>
					<th></th>
				</tr>
				<tbody>
					@foreach($tag->posts as $post)
					<tr>
						<th>{{ $post->id }}</th>
						<td>{{ $post->title }}</td>
						<td>
							@foreach($post->tags as $tag)
									<span class="label label-default">{{ $tag->name }}</span>
							@endforeach
						</td>
						<td><a href="{{ route('admin.posts.show', $post->id) }}"	class="btn btn-default btn-small">View</a></td>
					</tr>
					@endforeach
				</tbody>
			</thead>
		</table>
		</div>
	</div>
@endsection