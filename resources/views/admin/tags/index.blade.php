@extends('layouts.admin')

@section('title')
Beata Nykiel Photography - All Tags
@endsection

@section('content')

<div class="row">
	<div class="col-md-8">
	@include('layouts.partials.session-error')
	<h1>Tags</h1>
	<table class="table">
		<thead>
			<tr>
			<th>#</th>
			<th>Name</th>
			</tr>
		</thead>
		<tbody>
		@foreach($tags as $tag)
			<tr>
				<th>{{ $tag->id }}</th>
				<td><a href="{{ route('admin.tags.show', $tag->id) }}">{{ $tag->name }}</a></td>
			</tr>
		@endforeach
		</tbody>
	</table>
	</div>
	<div class="col-md-3">
		<div class="well">
		<h3>Create New Tag</h3>
			{!! Form::open(['route'=> 'admin.tags.store', 'method'=> 'POST']) !!}
			{{ Form::label('name', 'Name:') }}
			{{ Form::text('name', null, ['class' => 'form-control']) }}
			{{ Form::submit('Create New Tag', ['class'=>'btn btn-primary btn-block']) }}
			{!! Form::close() !!}
		</div>
	</div>
</div>


@endsection
