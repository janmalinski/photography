@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Admin Dasboard
@endsection

@section('content')
<section class="users">
	<h3 class="text-center">Users</h3>
	<hr>
	 <table class="table">
        <thead>
        <th>First Name</th>
  
        <th>E-Mail</th>
        <th>User</th>
        <th>Client</th>
        <th>Admin</th>
        <th></th>
        <th></th>
        </thead>
	        <tbody>
	        @foreach($users as $user)
		        <tr>
		        <form action="{{ route('admin.assign') }}" method="POST">
		        	<td>{{ $user->name }}</td>
		        	<td>{{ $user->email }} <input type="hidden" name="email" value="{{ $user->email }}"></td>
		        	<td><input type="checkbox" {{ $user->hasRole('User') ? 'checked' : ''}} name="role_user"></td>
		        	<td><input type="checkbox" {{ $user->hasRole('Client') ? 'checked' : ''}} name="role_client"></td>
		        	<td><input type="checkbox" {{ $user->hasRole('Admin') ? 'checked' : ''}} name="role_admin"></td>
		        	{{ csrf_field() }}
		        	<td><button class="btn btn-primary" type="submit">Assign Roles</button></td>
		        </form>
		        </tr>
		      @endforeach
	        </tbody>
    </table>
	</section>
@endsection