@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Edit Sessions Content
@endsection
@section('content')

	<section class="edit-videosession">
	<div class="row">
		<div class="col-xs-12">
		 <div class="panel panel-default">
		    <div class="panel-heading">
		              <strong>Edit Session Video</strong>
		            </div>  

								
						 {!! Form::model($content,[ 'method'=>'PUT','action'=>['SessionsController@update',$content->id]])  !!}
		        

		{!! Form::hidden('id', $content->id) !!}
		           
		      @include('layouts.partials.video-form')

		      {!! Form::close() !!}
		  </div>
		</div>
	</div>
	</section>

@endsection