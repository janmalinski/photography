@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Create Sessions Content
@endsection
@section('content')


       <div class="panel panel-default">
            <div class="panel-heading">
              <strong>Add Video</strong><br>
              <strong class="text-danger">The video shuould have embed version</strong>
            </div>  

			{!! Form::open(['route' => 'admin.sessions.store']) !!}
			
			@include('layouts.partials.video-form')

            {!! Form::close() !!}
          </div>



@endsection