@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - Sessions Content
@endsection


@section('content')
<style>
	.card{
	padding: 40px 0px;
	}
</style>

<section class="session-video">	

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			@include('layouts.partials.session-error')
		</div>	
	</div>
	<div class="row">
		<div class="col-xs-12">
		     <div class="panel panel-default">
			      <div class="panel-heading clearfix">
			        <div class="pull-left">
			          <h4>All Sessions Videos</h4>
			        </div>
			        {{-- @if(count($videos) === 0 ) --}}
			        <div class="pull-right">
			            <a href="{{ route('admin.sessions.create') }}" class="btn btn-success">
			              <i class="glyphicon glyphicon-plus"></i> 
			              Add Slideshow
			            </a>
			      </div>
			     {{--  @endif --}}
			  </div>
		</div>
	</div>

<div class="row">
	@foreach($videos as $content)
		<div class="col-xs-12">
			<div class="card">
				<div class="card-content text-center">
					<h3>{{ $content->title }}</h3>
					<p>{{ $content->description }}</p>
				</div>
				<div class="top-separator"></div>
				<div class="embed-responsive embed-responsive-16by9">
	   						<iframe width="560" height="315" src="{{ $content->url  }}" frameborder="0" allowfullscreen></iframe>
				</div>
	
				     <div class="edit-delete pull-right">
	                 
	                      <a href="{{ route('admin.sessions.edit', ['id'=>$content->id]) }}"  class="text-info"  title="Edit">
	                          Edit
	                     </a>
	                    |
	                    <a href="{{ route('admin.session.delete', ['id'=>$content->id] ) }}" class="text-danger">Delete</a>
	                    </div>
	                       
				</div>
			</div>
		@endforeach	
		</div>					
	</div>

	</section>


@endsection