
@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - List of Testimonal's Authors
@endsection
@section('content')
	
	<section class="subscribers-list">
	
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
				@include('layouts.partials.session-error')
				<h2>List of Authors</h2>
@foreach($authors  as $author)
				
					<p>{{ $author->name }}</p> 
			
				
					<p>{{ $author->email }}</p>
					<div class="clearfix"></div>
					<div class="pull-right">
					 <a href="{{ route('admin.authors.delete', ['id'=>$author->id] ) }}" class="text-danger">Delete</a>
					 </div>
					<hr>


@endforeach
      <div class="text-center">

    
            @if($authors->currentPage() !== 1)

              <a href="{{ $authors->previousPageUrl() }}"><span class="fa fa-caret-left"></span></a>

            @endif

            @if($authors->currentPage() !== $authors->lastPage()  && $authors->hasPages())

              <a href="{{ $authors->nextPageUrl() }}"><span class="fa fa-caret-right"></span></a>

            @endif

          
          </div>
				</div>
					

			</div>

		
	</section>


@endsection