@extends('layouts.admin')

@section('title')
Beata Nykiel Photography - Gallery
@endsection

@push('styles')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
  <link rel="stylesheet" href="{{ URL::asset('assets/css/gallery.css') }}" type="text/css"/>
  <link rel="stylesheet" href="{{ URL::asset('assets/css/lightbox.css') }}" type="text/css"/>
@endpush

@section('content')

<section class="image-upload"> 

  <div class="row">
    <div class="col-xs-12">

      <div id="gallery-images">
      <h3 class="text-center">{{ $gallery->title }}</h3>
        <ul>
        @foreach ($gallery->images as $image)
          <li>
            <a href="{{ url($image->file_path) }}" data-lightbox="mygallery">
            <img class="img-responsive center-block"  src="{{ url('gallery/thumbs/' .$image->file_name) }}" >
            </a>
          </li>
          @endforeach

        </ul>
      </div>
    </div>
   </div>


  <div class="row">
    <div class="col-md-12">
      <form action="{{ url('admin/image/do-upload') }}" 
      class="dropzone"
       id="addImages">

       {{ csrf_field() }}

       <input type="hidden" name="gallery_id" value="{{ $gallery->id }}">


        
      </form>
    </div>
   </div>


   <div class="row">
    <div class="col-md-12">
      <a href="{{ url('admin/gallery') }}" class="btn btn-primary">Back</a>
    </div>
   </div>

</section>  
   @endsection

@push('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js" ></script>
  <script  src="{{  URL:: asset('assets/js/lightbox.js') }}"></script>
  <script  src="{{  URL:: asset('assets/js/gallery.js') }}"></script>
@endpush
