@extends('layouts.admin')
@section('title')
Beata Nykiel Photography - List of Galleries
@endsection
@section('content')
<section class="gallery-list">

<div class="row">
	<div class="col-md-12">
		<h1>My Galleries</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		
			<table class="table table-striped table-bordered table-responsive">
				<thead>
					<tr>
						<th>Title of gallery</th>
						<th>Number of photos</th>
						<th>Front photo</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				@foreach($galleries as $gallery)
					<tr>
						<td>{{ $gallery->title }}</td>
						<td>{{ $gallery->images()->count() }}</td>
					
						<td><img class="center-block " src="{{ url('gallery/frontimage/480/' . $gallery->photo) }}"></td>
						<td ><a href="{{ route('admin.gallery.show', ['id'=>$gallery->id]) }}">View</a> /
						<a href="{{url('admin/gallery/delete/'. $gallery->id)}}">Delete</a>
						{{-- <a href="{{ route('admin.gallery.delete', ['id'=>$gallery->id]) }}">Delete</a> --}}
						</td>
					
					</tr>
				@endforeach
				</tbody>
			</table>
		
	</div>
	<div class="col-md-4">
	@include('layouts.partials.session-error')
	
		{!! Form::open(array('url'=>'admin/gallery','method'=>'POST', 'files'=>true)) !!}

          
			<div class="form-group">
			 {!! Form::text('title', null, ['class' => 'form-control', 'id'=>'title', 'placeholder'=>'Title of the gallery']) !!}
			</div>
			<div class="form-group">
				 {!! Form::textarea('description', null, ['class' => 'form-control', 'id'=>'description', 'placeholder'=>'Description of the gallery']) !!}
			</div>
			<div class="form-group">
			     {!! Form::text('slug', null, ['class' => 'form-control', 'id'=>'slug', 'placeholder'=>'Slug of the gallery']) !!}
			 </div>
			 <div class="form-group">
			      {!! Form::file('photo') !!}
			  </div>
			{!! Form::submit('Submit', array('class'=>'btn')) !!}
	      	{!! Form::close() !!}
			</div>
	</div>

</section>
@endsection

