@extends('layouts.app')
@section('title')
Beata Nykiel Photography - JSON Test
@endsection

@section('content')
<h1>Pokaż ksiazki</h1>

<a href="#" id="book-button">Wczytaj ksiazki</a>
<div id="book-list"></div>

@endsection
@push('scripts')


<script>
$(function(){

$('#book-button').on('click', function(e){

e.preventDefault();

$('#book-list').html('wczytywanie...');
$.get('books/books', function(data){
	var book_list = '';
	$.each(data, function(){
		var book_list = + this + '<br>';
	})
	$('#book-list').html('book_list');
	$('#book-button').hide();
});

});

});
</script>

@endpush
