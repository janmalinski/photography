@extends('layouts.app')
@section('title')
Beata Nykiel Photography - Galleries
@endsection
@push('header-scripts')
 <script>
    // Picture element HTML5 shiv
    document.createElement( "picture" );
  </script>
 <script src="{{ URL::asset('assets/js/picturefill.min.js') }}" async ></script>
@endpush

@section('content')


	    <section class="galleries">
		<div class="row">
		  @foreach($galleries as $gallery)

		  <?php  $photo = ! is_null($gallery->photo) ? $gallery->photo : '' ?>
		<div class="col-xs-12">

 		
 		{{-- 	
 			<div class="media"> --}}

 			<a href="galleries/{{  $gallery->slug }}">
		<img  class="lazyload img-responsive center-block galleries"   data-srcset=" gallery/frontimage/480/{{ $photo }} 480w, gallery/frontimage/768/{{ $photo }} 768w, gallery/frontimage/1140/{{ $photo }} 1140w"  alt="{{ $gallery->title }}" alt="{{ $gallery->title }}"> </a>

			{{--   <div class="media__body"> --}}
			  	<h3>{{ $gallery->title }}</h3>
			    <p>{{ $gallery->description }}</p>
		{{-- 	  </div> --}}
	{{-- 		</div> --}}

	
	
		

		</div>
		@endforeach

	
		</div>

	</section>


@endsection


@push('scripts')
<script src="{{ URL::asset('assets/js/lazysizes.min.js') }}"></script>
@endpush