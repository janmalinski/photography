<div id="testimonal-list">
		
		<article class="testimonal" id="{{ $testimonal->id }}">
					<blockquote>
				<p>{{ $testimonal->testimonal }}</p>
			
								
		
				<div class="media">
				  <div class="media-left media media-middle">
			
				<img class="img-circle media-object" id="preview" src="{{ url('testimonal/avatar/' . $testimonal->author->photo) }}" width="64px" height="64px">
				  
				  </div>
				  <div id="testimonal-body" class="media-body">
				 
								 <cite  ">{{ $testimonal->author->name  }}</cite> 
				  
				  </div>
				</div>
			
			</blockquote>
		</article>
</div>




	