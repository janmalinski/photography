@extends('layouts.app')
@section('title')
Beata Nykiel Photography - Sessions
@endsection
@section('content')
<section class="sessions">	

	<div class="row">
		<div class="col-xs-12">
			@foreach($videos as $content)
			<div class=" text-center">
					<h2>{{ $content->title }}</h2>
					<p>{{ $content->description }}</p>
				</div>
			@include('layouts.partials.session-error')
				<div class="top-separator"></div>
				<div class="embed-responsive embed-responsive-16by9">
	   						<iframe width="560" height="315" src="{{ $content->url  }}" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		@endforeach	
		</div>					


	</section>
@endsection

