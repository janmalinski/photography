@extends('layouts.app')
@section('title')
Beata Nykiel Photography - Contact
@endsection
@push('header-scripts')
 <script>
    // Picture element HTML5 shiv
    document.createElement( "picture" );
  </script>
 <script src="{{ URL::asset('assets/js/picturefill.min.js') }}" async ></script>
@endpush
@section('content')
<section class="contact-content">

   <div class="row">
     @foreach($contact_content as $content)
                  
    <?php $photo = ! empty($content->photo) ? $content->photo : 'placeholder.png' ?>
              <div class="col-xs-12">

 

<div class="separator"></div>
           <img class="img-responsive center-block"   srcset="contactcontent/1140/{{ $photo }} 1140w, contactcontent/1024/{{ $photo }} 1024w, contactcontent/480/{{ $photo }} 480w" title="{{ $content->title }}" alt="{{ $content->title }}">


<div class="separator"></div>

   
                </div>
    </div>


<div class="row">
    <div class="col-xs-12">
      <h3 class="text-center"> {{ $content->title }}</h3>
      <div class="separator"></div>


              <p>{{ $content->description }}</p>


      <div class="separator"></div>
    </div>
</div>
     @endforeach

  



    <div class="row">  
      <div class="col-md-6 col-md-offset-3">    
  
                <div class="panel-body">
                <div class="text-center">

@if(Session::has('flash_message'))

    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>

@endif




</div>
                    @include('layouts.partials.contact-form')
		            </div>
      </div>
		  </div>
 

</section>
@endsection
