@extends('layouts.app')
@section('title')
Beata Nykiel Photography - Galleries
@endsection
@push('styles')
 <link rel="stylesheet" href="{{ URL::asset('assets/css/lightbox.css') }}" type="text/css"/>
@endpush
@push('header-scripts')
 <script>
    // Picture element HTML5 shiv
    document.createElement( "picture" );
  </script>
 <script src="{{ URL::asset('assets/js/picturefill.min.js') }}" async></script>
@endpush
@section('content') 

<style type="text/css">
	
	img{
		margin-bottom:30px;
		max-width: 350px;
	}
	h3{
		text-transform: uppercase;
		margin-bottom: 20px;
	}
	p{
		margin-bottom: 40px;
	},,lk
</style>

	    <section class="gallery">
		<div class="row">
			<div class="col-xs-12">
			<h3 class="text-center">{{ $gallery->title }}</h3>
				
			<p>{{ $gallery->description }}</p>

			</div>
		</div>	
         <div class="row">
          @foreach ($gallery->images as $image)
			<div class="col-sm-4">
            <a href="{{ url($image->file_path) }}" data-lightbox="gallery">
            <img class="lazyload img-responsive center-block" data-src="{{ url('gallery/350/' . $image->file_name) }}" >
            
            </a>

        	</div>
        	@endforeach
    	</div>
	</section>

@endsection
@push('scripts')
<script src="{{URL::asset('assets/js/lazysizes.min.js') }}"></script>
 <script  src="{{URL::asset('assets/js/lightbox.js') }}"></script>
@endpush
