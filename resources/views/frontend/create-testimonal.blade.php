@extends('layouts.app')
@section('title')
Beata Nykiel Photography - Add Testimonal
@endsection
@push('header-scripts')
 <script>
    // Picture element HTML5 shiv
    document.createElement( "picture" );
  </script>
<link  href="{{ URL::asset('assets/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
{{--   <script src="{{ URL::asset('assets/css/custom.css') }}" async ></script> --}}
  <link  href="{{ URL::asset('assets/css/form-circular-photo.css') }}"  rel="stylesheet" type="text/css">
@endpush

@section('content')



	
@if(count($testimonals)>0)

			<h2 class="text-center">Latest Testimonals</h2>

@endif

<div class="row">
	<div class="col-md-8 col-md-offset-2">




			@foreach($testimonals as $testimonal)

	

		@include('frontend.testimonal-item', compact('testimonal'))

	
			@endforeach
		</div>
	</div>




		
<div class="row">
<div class="col-xs-12">
	@include('layouts.partials.testimonal-form')
</div>
</div>

	

         

{{-- 
		  <div class="text-center">

    
            @if($testimonals->currentPage() !== 1)

              <a href="{{ $testimonals->previousPageUrl() }}"><span class="fa fa-caret-left"></span></a>

            @endif

            @if($testimonals->currentPage() !== $testimonals->lastPage()  && $testimonals->hasPages())

              <a href="{{ $testimonals->nextPageUrl() }}"><span class="fa fa-caret-right"></span></a>

            @endif

          
          </div>
  --}}


@endsection


@push('scripts')

 <script src="{{ URL::asset('assets/js/jasny-bootstrap.min.js')}}"></script>


 <script>




$("#click").click(function(event){
event.preventDefault();

// reset errors
$("#upload_form").find('.help-block').remove();
$("#upload_form").find('.form-group').removeClass('has-error');

$.ajax({
      url:"{{ route('testimonal.store') }}",
    data:new FormData($("#upload_form")[0]),
      type:'post',
      processData: false,
      contentType: false,
      success:function(data){

 
 $data =$(data);
 $('#testimonal-list').html($data).prepend('<div class="alert alert-success text-center">Thank you for testimonal!</div>')
 function relocate(){
 window.location.replace("/testimonal-created");
}
setTimeout(relocate, 5000);
  
    },

      error: function(xhr){
      	var errors = xhr.responseJSON;
      	if($.isEmptyObject(errors) == false){
      		$.each(errors, function(key, value){
      				$('#' + key)
      				.closest('.form-group')
      				.addClass('has-error')
      				.append('<span class="help-block"><strong>' + value + '</strong></span>')
      		});
      	}

      }
    });
 });





 </script>

@endpush