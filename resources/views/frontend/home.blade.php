@extends('layouts.app')
@section('title')
Beata Nykiel Photography
@endsection
@push('header-scripts')
  <script>
     // Picture element HTML5 shiv
    document.createElement( "picture" );
  </script>

 <script src="{{ URL::asset('assets/js/jquery.bxslider.min.js') }}"></script>  

 <script src="{{ URL::asset('assets/js/picturefill.min.js') }}" async ></script>
@endpush

@push('styles')
<link href="{{ URL::asset('assets/css/jquery.bxslider.css') }}" rel="stylesheet" >

@endpush


@section('content')

<style>
section{
  padding: 30px 0px;
}
</style>


          @include('layouts.partials.home-slideshow')

  
 <div class="separator"></div>





@if(count($home_content) > 0)
@foreach($home_content as $content)
<section class="introduction">
           <div class="row">
              <div class="col-xs-12">
                     <h3 class="text-center">{{ $content->introduction_title }}</h3>
                            <h4>{{ $content->introduction}}</h4>
                 </div>
             </div>    
</section>
<section class="advantages">
    <div class="row">
              <div class="col-xs-12">
                            <h3 class="text-center">{{ $content->advantages_title }}</h3>
                            <h4>{{ $content->advantages}}</h4>
                 </div>
             </div>
</section>
<section class="how-i-work">
    <div class="row">
              <div class="col-xs-12">
                            <h3 class="text-center">{{ $content->how_i_work_title }}</h3>
                            <h4>{{ $content->how_i_work}}</h4>
                 </div>
             </div>
</section>
@endforeach
@endif

  



@endsection

@push('scripts')
<script src="{{ URL::asset('assets/js/home.js') }}"></script>  
@endpush





