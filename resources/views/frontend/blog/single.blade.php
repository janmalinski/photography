@extends('layouts.app')
@section('title')

<?php $titleTag = htmlspecialchars($post->title); ?> 

{{-- lub: {{ $post->title }} --}}
<?php echo 'Beata Nykiel Photography - '.$titleTag; ?>

@endsection
@push('styles')
<link href="{{ URL::asset('assets/css/jquery.bxslider.css') }}" rel="stylesheet" >
@endpush
@push('header-scripts')
 <script>
    // Picture element HTML5 shiv
    document.createElement( "picture" );
  </script>
 <script href="{{ URL::asset('assets/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet"></script>

@endpush


<style>

.input-group-btn > .btn-default {
    padding-top: 10px!important;
    padding-bottom: 9px!important;
}


.comment{
    margin-bottom: 45px;
}


.author-image{
    width: 50px;
    height: 50px;
    border-radius: 50%;
    float: left;
}

.author-name{
float:left;
margin-left: 15px;
}

.author-name>h4{
    margin: 5px 0px;
}

.author-time{
    font-size:11px;
    font-style: italic; 
    color:#aaa; 
}


.comment-content{
    clear: both;
    margin-left: 65px;
    font-size: 16px;
    line-height: 1.3em;
}

.comments-title{
    margin-bottom: 45px;
}

.testimonals p{
    font-size: 11px; 
    font-style: italic;
}

.nested-comment{
    margin-top: 60px;
}

.comment-reply{
    display: none;
}

.media{
    padding: 20px 0px;
}

.media-right{
    padding-left: 50px!important;
}

a.toggle-reply{
    cursor: pointer;

}

</style>
           
@section('content')
 
<section class="front-image">
@if(count($post->post_images)>0)
        <img class="img-responsive center-block" src="{{ url('post/1920/' . $Fimage->file_name) }}" alt="">
@endif
</section>
    <div class="row">
        <div class="col-md-7">
     
            <h1> {{ $post->title }}</h1>
            <h4> by: {{ $post->user->name }}</h4>
            <p>{{ strip_tags($post->body) }}</p>
            <p>Posted: {{ date('M j, Y', strtotime($post->created_at)) }}</p>
            <p>In: {{ $post->category->name }}</p>
            @foreach ($post->post_images as $image)  
            @if ($image->file_name !== $Fimage->file_name)
            <img class="lazyload img-responsive center-block" data-srcset="{{ url('post/850/' . $image->file_name) }}" >
            @endif
            @endforeach 
                          
             <!-- Comments Form -->
             @include('layouts.partials.session-error')
             <h3>Comments</h3>
          <hr>
@if(!Auth::check()) 
        <div class="well">
                
         
                     
                       
                        <h4 class="text-danger">You have to be logged in to write a comment or reply to comment!</h4>
                 
        </div>
@endif



@include('layouts.partials.comment.comment-form')



<!-- Begin comment List -->
@if(count($post->comments) > 0)
@include('layouts.partials.comment.comments-list', ['collection'=> $comments['root']])
@endif




</div>
 <div class="col-md-3 col-md-offset-1">     
<section class="sidebar">
<section class="search-post">
                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input name="term" type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>
</section>
<section class="book-your-session">
    <h3 class="text-center">Book Your Session</h3>
</section>
<section class="categories">
<h3 class="text-center">Categories</h3>
    @if(count($categories)>0)

             <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Blog Categories</h4>
                   
                    <div class="row">
  @foreach($categories as $category)
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">{{ $category->name }}</a>
                              
                            </ul>
                        </div>
   @endforeach 
                    </div>
                    <!-- /.row -->
                </div>
    @endif
</section>
<section class="tags">
<h3 class="text-center">Tags</h3>
    @if(count($tags)>0)
    @foreach($tags as $tag)
        
        <a href="">{{ $tag->name }}</a>
    
    @endforeach
    @endif
</section>
<section class="testimonals">
<h3 class="text-center">Testimonals</h3>
    @foreach($testimonals as $testimonal)

        <article class=testimonal>
                <blockquote>
                <p>{{ $testimonal->testimonal }}</p>
                <div class="media">
                  <div class="media-left media media-middle">
                <img class="img-circle media-object" src="{{ url('testimonal/avatar/' . $testimonal->author->photo) }}" width="64px" height="64px">
                  </div>
                  <div class="media-body">
                    <footer>
                    <cite>{{ $testimonal->author->name  }}</cite>
                    </footer>
                  </div>
                </div>
                </blockquote>
        </article>
        
    @endforeach
</section>
    </section>
    </div>
    </div>





@endsection
@push('scripts')
<script src="{{URL::asset('assets/js/lazysizes.min.js') }}"></script>
<script src="{{URL::asset('assets/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
    //  $('.comment-reply-container').find('.toggle-reply').click(function(){
     
    //     $(this).next().slideToggle('slow');
 
    // });
$(function(){

    $("input[name=term]").autocomplete({

            source: "{{ route("blog.autocomplete") }}",
            minLength: 3,
            select: function(event, ui) {
                $(this).val(ui.item.value);
            }


        });


});
</script>
@endpush