@extends('layouts.app')
@section('title')
Beata Nykiel Photography - Blog
@endsection

@section('content')

	


<div class="row">
	 <div class="col-md-12">
	 	<h1 class="text-center">
	 		Blog
	 	</h1>
	 </div>
</div>
 <section class="post">
@foreach($posts as $post)
	      <div class="row">
        <div class="col-md-6">

@if(count($post->image)>0)
	<a href="{{ route('blog.single', $post->slug) }}">
	<img class="img-responsive post-image" src="{{ asset('post/frontimage/'.$post->image) }}" alt="{{ $post->title }}" width="555">
	</a>
        </div>
	        <div class="col-md-6">
	          <div class="post-text">
	            <h2>{{ $post->title }}</h2>
	            <h5>Published: {{ date('M j, Y', strtotime($post->created_at)) }}</h5>
	            {{-- stri_tags -usuwa tagi html z bazy danych dajac tekst --}}
	            <p>{{ substr(strip_tags($post->body), 0, 300) }}{{ strlen(strip_tags($post->body))>300 ? '...' : '' }}</p>
	            <a class="text-primary" href="{{ route('blog.single', $post->slug) }}" >Read More</a>
	          </div>
@endif


	</div>
	</div>
@endforeach
</section>
	<div class="row">
		<div class="col-md-12">
			<div class="text-center">
				{!! $posts->links() !!}
			</div>
		</div>
	</div>


	      
	       





@endsection