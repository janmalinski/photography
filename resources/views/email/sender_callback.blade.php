@extends('layouts.email')


@section('content')

<section class="sender-callback">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			<h1>Thank you for sing up {{  $name }}.</h1>
			

			<p>You will receive my newsletter once a month</p>
	    
			<p>Regards</p>		
			<p>Beata</p>
			</div>
		</div>		
	</div>
</section>

@endsection