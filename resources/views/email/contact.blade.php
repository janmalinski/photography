<h3>You have a New Contact via the Contact Form</h3>

<h4>From: {{ $sender }}</h4>
<h4>{{ $subject }}</h4>
<div>
{{ $bodyMessage }}
</div>

<p>Sent via {{ $email }}</p>