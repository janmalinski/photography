@extends('layouts.app')
@push('header-scripts')
<link  href="{{ URL::asset('assets/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
{{--   <script src="{{ URL::asset('assets/css/custom.css') }}" async ></script> --}}
  <link  href="{{ URL::asset('assets/css/form-circular-photo.css') }}"  rel="stylesheet" type="text/css">
@endpush
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('signup') }}"   enctype="multipart/form-data">
                        {{ csrf_field() }}
                 <div class="col-md-4 col-md-offset-3">
                            <div class="form-group"{{ $errors->has('name') ? ' has-error' : '' }}>
                                <label class="control-label">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        <div class="form-group"{{ $errors->has('email') ? ' has-error' : '' }}>
                            <label class="control-label">E-Mail Address</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                     @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
               
                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="control-label">Password</label>

                       
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                          
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="control-label">Confirm Password</label>

                        
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                         
                        </div>
                    </div>
           <div class="col-md-1">
        
          <div class="fileinput fileinput-new" data-provides="fileinput" {{ $errors->has('photo') ? ' has-error' : '' }}>
                <div class="fileinput-new thumbnail" style="width: 150px; height: 150px;">
             <?php $photo = ! empty($user->photo) ? $user->photo : 'https://placehold.it/150x150' ?>
             
                        <img  src="https://placehold.it/150x150" alt="Photo">
                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style="width: 150px; height: 150px;;"></div>
                         @if ($errors->has('photo'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('photo') }}</strong>
                                    </span>
                                @endif 
                      <div class="text-center">
                        <span class="btn btn-default btn-file"><span class="fileinput-new">Choose Photo<br>(optional)</span><span class="fileinput-exists">Change</span><input type="file" name="photo"></span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>

                 
                  </div>
                </div>
   
        
              <div class="row">
                <div class="col-md-4 col-md-offset-3">
                      <button type="submit" class="btn  btn-primary">Save</button>
                </div>
              </div>                        
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
                 
@endsection
@push('scripts')

 <script src="{{ URL::asset('assets/js/jasny-bootstrap.min.js')}}"></script>

@endpush