<?php

namespace App;



use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        // these last 3 settings optional here  just to explain 
        return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
    }

    public function hasAnyRole($roles){
        // gdy ma wiele rols = is_array
           if(is_array($roles)){
            foreach($roles as $role){
                if($this->hasRole($role)){
                    return true;
                }
            }
        // tu dla jednej roli ale argumet $roles bo nie loop
           }else{
           if($this->hasRole($roles)){
                    return true;
           }
        }
        return false;
    }


// chceking if user ma specific role
  public function hasRole($role)
  {
        if($this->roles()->where('name', $role)->first()){
            return true;
        }
       return false;
  }


  public function posts(){
    return $this->hasMany('App\Post');

  }

}
