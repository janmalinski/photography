<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactContent extends Model
{
  	protected $table = 'contact_content';

	protected $fillable = ['title','description','photo'];
}
