<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sender extends Model
{

    protected $table="senders";

    protected $fillable = array(
	'name',
	'email'
	);


       public function contact_messages()
    {

    	return $this->hasMany('App\ContactMessage');
    	
    }
}
