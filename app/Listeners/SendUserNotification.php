<?php

namespace App\Listeners;

use App\Events\TestimonalCreated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;



class SendUserNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TestimonalCreated  $event
     * @return void
     */
    public function handle(TestimonalCreated $event)
    {
        $author = $event->author_name;
        $email = $event->author_email;

        Mail::send('email.user_notification', ['name'=> $author, 'email'=>$email], function($message) use ($email, $author){
            $message->from('jan.malinski81@gmail.com', 'Admin');
            $message->to($email, $author);
            $message->subject('Thank you for your Testimonal!');
        });
    }
}
