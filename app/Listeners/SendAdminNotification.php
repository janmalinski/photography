<?php

namespace App\Listeners;

use App\Events\TestimonalCreated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;

class SendAdminNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TestimonalCreated  $event
     * @return void
     */
    public function handle(TestimonalCreated $event)
    {
         $author = $event->author_name;
        $email = $event->author_email;

        Mail::send('email.admin_notification', ['name'=> $author, 'email'=> $email], function($message) use ($email, $author){
            $message->from('jan.malinski81@gmail.com', 'Admin');
            $message->to('beata.nykiel@gmail.com');
            $message->subject('The Testimonal has been created!');
        });
    }
}
