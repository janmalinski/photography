<?php

namespace App\Listeners;

use App\Events\ContactCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSenderNewsletterCancellation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ContactCreated  $event
     * @return void
     */
    public function handle(ContactCreated $event)
    {
       $sender = $event->sender_name;
        $email = $event->sender_email;
      

        Mail::send('email.sender_newsletter_cancellation', ['name'=> $sender, 'email'=>$email,], function($message) use($email, $sender)
        {
            $message->from('beata.nykiel@gmail.com', 'Admin');
            $message->to($email,$sender);
            $message->subject('You can cancell subscription at any time');

        } );

    }
}
