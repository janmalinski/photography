<?php

namespace App\Listeners;

use App\Events\ContactCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAdminConfirmation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct($sender)
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ContactCreated  $event
     * @return void
     */
    public function handle(ContactCreated $event)
    {
        $sender = $event->sender_name;
        $sender_email = $event->sender_email;
        $website_email = 'contact@beatanykiel.com';

        Mail::send('email.admin_confirmation', ['name'=> $sender, 'email'=>$sender_email], function($message) 
            use($sender, $sender_email, $website_email)
        { 
            $message->from('beata.nykiel@gmail.com', 'Admin');
            $message->to($website_email,$sender_email,$sender);
            $message->subject('You have recived message!');
                 } );
    }
}
