<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactMessage extends Model
{
  	protected $table = 'contact_messages';

	protected $fillable = ['phone', 'subject', 'message'];

 public function sender()
    {
    	return $this->belongsTo('App\Sender');
    }

}
