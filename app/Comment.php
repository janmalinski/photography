<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Comment extends Model

{


	protected $fillable = [

	'body',
    'user_id',
    'post_id',
    'parent_id',
    'is_active'

	];


    public function post()
    {
        return $this->belongsTo('App\Post');
    }


    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function newCollection(array $models = [])
    {
        return new CommentCollection($models);
    }

    public function replies()
    {
        return $this->hasMany('App\Comment', 'parent_id');
    }



}
