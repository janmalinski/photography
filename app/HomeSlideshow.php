<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeSlideshow extends Model
{
   //custom name of table
    protected $table="home_slideshow";

 protected $fillable = array(

	'title', 
	'description',
	'photo',
	'order'
	
	);

}
