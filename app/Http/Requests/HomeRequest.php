<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
    return [
    'introduction_title'=> 'required |  min:5', 
    'introduction'=> 'required |  min:5',
    'advantages_title'=> 'required |  min:5',
    'advantages'=> 'required |  min:5',
    'how_i_work_title'=> 'required |  min:5',
    'how_i_work' => 'required |  min:5'      
     ];
    }
}
