<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactContentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

          return [
             'title'=>   'required|max:255',
             'description'=> 'required|max:500',  
             'photo' => 'mimes:jpg,jpeg,png,gif,bmp'
            
        ];


 // if ($this->method() == 'PUT')
 //  {
 //    // Update operation, exclude the record with id from the validation:

 //    $update_title_rule = 'min:5';

 //    // $update_photo_rule = 'required|unique:contact_content,photo'. $this->get('id');

 //    return [
 //             'title'=>  $update_title_rule,
 //             'description'=> 'max:500',  
 //             // 'photo'=> 'required'
 //        ];
 //  }
 //  else
 //  {
 //    // Create operation. There is no id yet.
 //    $title_rule = 'required|min:5|unique:contact_content,title';
 // }
 //        return [
 //             'title'=>  $title_rule ,
 //             'description'=> 'max:500',
 //             'photo'=> 'required'
            
 //        ];
    }

}
