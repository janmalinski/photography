<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeSlideshowRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {



        return [
           'title' => 'required|min:5',
           'description' => 'required|max:500',
           'order' =>  'required|numeric',
           'photo' => 'mimes:jpg,jpeg,png,gif,bmp'
      
        ];



// To Consider
     

 // if ($this->method() == 'PUT')
//   {
//         $photo_rule = 'required|mimes:jpg,jpeg,png,gif,bmp|unique:home_slideshow,'. $this->get('id');
//         $order_rule = 'required|numeric|unique:home_slideshow,'. $this->get('id');
      
//   }
//   else
//   {

//        $photo_rule = 'required|mimes:jpg,jpeg,png,gif,bmp|unique:home_slideshow';
//        $order_rule = 'required|numeric|unique:home_slideshow';
// }


//       return [
//            'title' => 'required| min:5',
//            'description' => 'max:500',
//            'photo' => $photo_rule,
//            'order' => $order_rule
      
//         ];
  
  }
}




