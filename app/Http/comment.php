@if($comment->comment_parent)
  <div class="media comment-item" data-comment-id="{{ $comment->id }}">
@else
  <li class="media comment-item" data-comment-id="{{ $comment->id }}">
@endif

  <a class="pull-left" href="#">
    <img class="media-object" style="width: 64px; height: 64px;" src="#">
  </a>

  <div class="media-body">
    <h4 class="media-heading">{{ $comment->author }}</h4>
    <p style='text-align:justify;'>{{ $comment->body }}</p>
    <p><a class='comment_reply_to' href='#writecomment' data-replyto='{{ $comment->id }}'>Reply</a></p>
    @if($comment->replies->count())
      {{ ($comment->replies) }}
    @endif
  </div>

@if($comment->comment_parent)
  </div>
@else
  </li>
@endif