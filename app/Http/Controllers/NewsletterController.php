<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ContactLog;

use App\Sender;

use Illuminate\Support\Facades\Session;

class NewsletterController extends Controller
{
    public function index()
    {


    	$subscribers = ContactLog::orderBy('id', 'desc')->paginate(10);;

    	
    	return view('admin.newsletter.index', compact('subscribers'));
    }

    public function getDeleteNewsletter($id)
{
        $subscriber = ContactLog::find($id);
        $subscriber->delete();

       Session::flash('flash_message', 'Subscriber has been deleted!');
       return redirect('admin/newsletter');

}


}
