<?php

namespace App\Http\Controllers;

use App\Video;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\VideoRequest;

use Illuminate\Support\Facades\Session;


class SessionsController extends Controller
{
    public function getSessions()
    {
      $videos = Video::all();
    	return view('frontend.sessions', compact('videos'));
    }

    public function index()
    {
    	$videos = Video::orderBy('id', 'asc')->get();
    	return view('admin.sessions.index', compact('videos'));
    }
  public function create()
    {
    return view('admin.sessions.create');
    }

    public function store(VideoRequest $request)
    {
    	Video::create($request->all());
    	Session::flash('flash_message', 'Session Content has been created!');
		return redirect('admin/sessions');

    }

   public function edit($id)
   {
   	$content = Video::find($id);

   	return view('admin.sessions.edit', compact('content'));
   } 

   public function update($id, VideoRequest $request)
   {
   	$content = Video::find($id);
   	$content->update($request->all());
   	Session::flash('flash_message', 'Session Content has been updated!');
	return redirect('admin/sessions');

   }


public function getDeleteVideoSession($id)
{
	   	$content = Video::find($id);
	   	$content->delete();
	   	$content->delete();
		Session::flash('flash_message', 'Session Content has been deleted!');
		return redirect('admin/sessions');
}

}
