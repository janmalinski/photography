<?php

namespace App\Http\Controllers;

use App\Gallery;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\GalleryRequest;

use App\Http\Requests\ImageRequest;

use Intervention\Image\Facades\Image;



use Illuminate\Support\Facades\Session;

   class GalleryController extends Controller
{

  
	private $upload_dir =  '/public/gallery';

	public function __construct()
	{
		$this->upload_dir = base_path() . '/' . $this->upload_dir;

	}


    public function getGalleries()
    {
      $galleries = Gallery::all();
    	return view('frontend.galleries', compact('galleries'));
    }

    public function getGallery($slug)
    {

         $gallery = Gallery::whereSlug($slug)->first();
         
       

        return view('frontend.gallery', compact('gallery'));
  


    }
    public function index()
    {


    	$galleries = Gallery::orderBy('id', 'asc')->get();
    	return view('admin.gallery.index', compact('galleries'));
    }

 

    public function store(GalleryRequest $request)
    {
     $data = $this->getRequest($request);
            Gallery::create($data);
            return redirect('admin/gallery');
    }

 private function getRequest(Request $request)
    {
       $data = $request->all();

     if ($request->hasFile('photo'))
        {
            $photo = $request->file('photo');
            $filename = $photo->getClientOriginalName();

            if(!file_exists('gallery')) {
             mkdir('gallery', 0777, true);
            }
         

            $destination = $this->upload_dir.'/frontimage';
            $photo->move( $destination,$filename);


            // mobile_size
        if(!file_exists('gallery/frontimage/480')) {
        mkdir('gallery/frontimage/480', 0777, true);
    }
    $mobile_size = Image::make('gallery/frontimage/' . $filename)->resize(480,  null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('gallery/frontimage/480/' . $filename, 100);




      if(!file_exists('gallery/frontimage/768')) {
        mkdir('gallery/frontimage/768', 0777, true);
    }
    $tablet_size = Image::make('gallery/frontimage/' . $filename)->resize(768,  null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('gallery/frontimage/768/' . $filename, 100);

// grid_size
        if(!file_exists('gallery/frontimage/1140')) {
        mkdir('gallery/frontimage/1140', 0777, true);
    }
    $desktop_size = Image::make('gallery/frontimage/' . $filename)->resize(1140,  null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('gallery/frontimage/1140/' . $filename , 100);

            $data['photo'] = $filename;
        }

        return $data;
    }





 public function doImageUpload(Request $request)
 {
    //get the file from post request - dropzone has name file
if ($request->hasFile('file')){


    $file = $request->file('file');

    // set my file name
    $filename =  $file->getClientOriginalName();

     $destination = $this->upload_dir;

    // move the file to correct location
    if(!file_exists('gallery')) {
        mkdir('gallery', 0777, true);
    }

    $file->move($destination, $filename);



    if(!file_exists('gallery/thumbs')) {
        mkdir('gallery/thumbs', 0777, true);
    }

    $thumb = Image::make('gallery/' . $filename)->resize(240, null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('gallery/thumbs/' . $filename, 100);

  if(!file_exists('gallery/350')) {
        mkdir('gallery/350', 0777, true);
    }
// galleries_size
    $galleries_size = Image::make('gallery/' . $filename)->resize(350,  null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('gallery/350/' . $filename , 100);


   

}
    

    // save the image details into the database
    $gallery = Gallery::find($request->input('gallery_id'));


    $image = $gallery->images()->create([

        'gallery_id'=> $request->input('gallery_id'),

        'file_name' => $filename,

        'file_path' => 'gallery/' . $filename,

        'small_photo_path'=> 'gallery/350/' . $filename,

        'thumb_photo_path'=> 'gallery/thumbs/' . $filename,

        ]);



    return $image;


 }


private function removePhoto($photo)
    {
        if( ! empty($photo))
        {

            $file_path =  $this->upload_dir . '/frontimage/' . $photo;

            if(file_exists($file_path)) unlink($file_path);


            $mobile_size_path =  $this->upload_dir . '/frontimage/480/' . $photo;

            if(file_exists($mobile_size_path)) unlink($mobile_size_path);


            $tablet_size_path =  $this->upload_dir . '/frontimage/768/' . $photo;

            if(file_exists($tablet_size_path)) unlink($tablet_size_path);


            $desktop_size_path =  $this->upload_dir . '/frontimage/1140/' . $photo;

            if(file_exists($desktop_size_path)) unlink($desktop_size_path);

        }
    }




 public function getDeleteGallery($id)
{
    $currentGallery = Gallery::findOrFail($id);

    $images = $currentGallery->images();

    foreach($currentGallery->images as $image){
      if (file_exists($image->file_path))unlink(public_path($image->file_path));
      if (file_exists($image->small_photo_path))unlink(public_path($image->small_photo_path));
      if (file_exists($image->thumb_photo_path))unlink(public_path($image->thumb_photo_path));

    }

    $this->removePhoto($currentGallery->photo);

    $images->delete();

    $currentGallery->delete();

    return redirect('admin/gallery');
}



    public function show($id)
    {

    	$gallery = Gallery::findOrFail($id);
    	return view('admin.gallery.show', compact('gallery'));
    }


}
