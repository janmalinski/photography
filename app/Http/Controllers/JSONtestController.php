<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class JSONtestController extends Controller
{
    public function getIndex()
    {
    	return view('books.index');
    }

    public function getBooks()
    {
    	$books = array(
'Alicja w krainei czarów',
'Factotum'
    	);

    	return Response::json($books);
    }
}
