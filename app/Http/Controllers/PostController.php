<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;

use App\Comment;

use App\Category;

use App\Tag;

use Session;

use Purifier;

use Image;

use Storage;



class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    private $upload_dir =  '/public/post';

    public function __construct()
    {
        $this->upload_dir = base_path() . '/' . $this->upload_dir;

    }



    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(10);
        $comment = Comment::all();
        return view('admin.posts.index', compact('posts', 'comment'));
    }

    /**
     * Show the form for creating a new resource.
     *gm
     * @return \Illuminate\Http\Response99
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.posts.create', compact('categories','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // die and dump - laravel function



        $this->validate($request, array(

            'title'=> 'required|max: 255',
            'slug'=> 'required|alpha_dash|min:5|max:255|unique:posts,slug',
            'user_id'=> 'required|integer',
            'category_id'=> 'required|integer',
            'body'=> 'required',
            'image'=>'sometimes|image'
            ));

        $post = new Post;

        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->user_id = $request->user_id;
        $post->category_id = $request->category_id;
        $post->body = Purifier::clean($request->body);


        if($request->hasFile('image')){
            $image = $request->file('image');
            // Intervention Image getClientOriginalExtension();
            $filename = time(). '.' . $image->getClientOriginalExtension();
            $location = public_path('post/frontimage/' .$filename);


            Image::make($image)->resize(555, 295, 
            function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();})
            ->save($location);

            //    file name to grab image
             $post->image = $filename;
        }
        $post->save(); 
          // po save gdy mamy many to many!!!
        // zawsze trzeba wpisac false by nadpisac istniejace asocjacje w tabeli

        $post->tags()->sync($request->tags, false);

        Session::flash('flash_message', 'Post has been saved!');
        return redirect()->route('admin.posts.show',$post->id);
    }

public function doPostImageUpload(Request $request)
 {
    //get the file from post request - dropzone has name file
if ($request->hasFile('file')){


    $file = $request->file('file');

    // set my file name
    $filename =  $file->getClientOriginalName();

     $destination = $this->upload_dir;

    // move the file to correct location
    if(!file_exists('post')) {
        mkdir('post', 0777, true);
    }

    $file->move($destination, $filename);

 if(!file_exists('post/thumb')) {
        mkdir('post/thumb', 0777, true);
    }

    $thumbSize = Image::make('post/' . $filename)->resize(242, null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('post/thumb/' . $filename, 100);


    if(!file_exists('post/480')) {
        mkdir('post/480', 0777, true);
    }

    $xsmallSize = Image::make('post/' . $filename)->resize(480, null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('post/480/' . $filename, 100);

  if(!file_exists('post/768')) {
        mkdir('post/768', 0777, true);
    }

    $smallSize = Image::make('post/' . $filename)->resize(768,  null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('post/768/' . $filename , 100);


  if(!file_exists('post/850')) {
        mkdir('post/850', 0777, true);
    }

    $mediumSize = Image::make('post/' . $filename)->resize(850,  null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('post/850/' . $filename , 100);


  if(!file_exists('post/1170')) {
        mkdir('post/1170', 0777, true);
    }
// galleries_size
    $largeSize = Image::make('post/' . $filename)->resize(1170,  null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('post/1170/' . $filename , 100);

      if(!file_exists('post/1920')) {
        mkdir('post/1920', 0777, true);
    }
// galleries_size
    $xlargeSize = Image::make('post/' . $filename)->resize(1920,  null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('post/1920/' . $filename , 100);


   

}
    

    // save the image details into the database
    $post = Post::find($request->input('post_id'));


    $image = $post->post_images()->create([

        'post_id'=> $request->input('post_id'),

        'file_name' => $filename,

        'file_path' => 'post/' . $filename,

        'thumb_photo_path'=> 'post/thumb/' . $filename,

        'xsmall_photo_path'=> 'post/480/' . $filename,

         'small_photo_path'=> 'post/768/' . $filename,

          'medium_photo_path'=> 'post/850/' . $filename,

           'large_photo_path'=> 'post/1170/' . $filename,

        'xlarge_photo_path'=> 'post/1920/' . $filename,

        ]);

    return $image;


 }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // $post = Post::where('id','=', $id)->first();
         //  the same 
        $post = Post::find($id);

        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $post = Post::find($id);
            $categories = Category::all();
            $cats = array();
            foreach($categories as $category){
                //               key = value
                $cats[$category->id] = $category->name;
            }
            $tags = Tag::all();
            $tags2 = array();
            foreach($tags as $tag){
                $tags2[$tag->id] = $tag->name;
            }
        return view('admin.posts.edit', compact('post','cats','tags2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
            $this->validate($request, array(
            'title'=> 'required|max: 255',
            // dodajemy "$id" w cudzysłowie do validacji unique bz uniqnc bedu walidacji + zastepuje powyzsze zakomentowane! 
            'slug'=> "required|alpha_dash|min:5|max:255|unique:posts,slug,$id",
            'category_id'=> 'required|integer',
            'body'=> 'required',
            'image'=>'image'
 )); 
 
        $post = Post::find($id);

        $post->title = $request->input('title');
        $post->slug = $request->input('slug');
        $post->category_id = $request->input('category_id');

        $post->body = Purifier::clean($request->input('body'));


        if($request->hasFile('image')){
            // ważna kolejnośc
            // add the new photo
             $image = $request->file('image');
            // Intervention Image getClientOriginalExtension();
            $filename = time(). '.' . $image->getClientOriginalExtension();
            $location = public_path('post/frontimage/' .$filename);


            Image::make($image)->resize(600, null, 
            function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();})
            ->save($location);

            $oldFilename = $post->image;

            // update the database
             $post->image = $filename;

            // Delete the old photo
            Storage::delete($oldFilename);
        }


        $post->save();

        // true bo nadpisujemy przez wymazanie i zapisanie od nowa !!
        // gdy wymazujemy wszystkie tagi to pojawia sie bład prz update bo array jest pusta tak jakby nie było $request->tags , dlatego czebo napisać if.. by zapobiecd temu
        if(isset($request->tags)){
            $post->tags()->sync($request->tags, true);
        } else {
            // wykasowanie poprzednich relacji w bazie danych gdy nic nie przekazujemy - wymazemy - dlatego przekazujemy pusta array()
            $post->tags()->sync(array());
        }

        Session::flash('flash_message', 'Post has been updated!');
        return redirect()->route('admin.posts.show',$post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDeletePost($id)
    {
        $post = Post::findOrFail($id);

          $post_images =  $post->post_images();

    foreach($post->post_images as $image){

       if (file_exists($image->file_path))unlink(public_path($image->file_path));
       if (file_exists($image->thumb_photo_path))unlink(public_path($image->thumb_photo_path));
       if (file_exists($image->xsmall_photo_path))unlink(public_path($image->xsmall_photo_path));
       if (file_exists($image->small_photo_path))unlink(public_path($image->small_photo_path));
       if (file_exists($image->medium_photo_path))unlink(public_path($image->medium_photo_path));
       if (file_exists($image->large_photo_path))unlink(public_path($image->large_photo_path));
       if (file_exists($image->xlarge_photo_path))unlink(public_path($image->xlarge_photo_path));
    }


        // gdy mamy belongsToMany = belongsToMany by wymazac zalezności
        // remove any reference of posts to post and tag models


         $post->tags()->detach();

         $post->comments()->delete();

       $post_images->delete();



       //usuwamy frontimage
       Storage::delete($post->image);

      $post->delete();

        Session::flash('flash_message', 'Post has been deleted!');
        return redirect()->route('admin.posts.index');
    }
}
