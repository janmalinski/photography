<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\User;
use App\Role;

use Auth;

use Illuminate\Foundation\Auth\ResetsPasswords;


use Image;






class AuthController extends Controller{

    private $upload_dir =  '/public/user/';

        public function __construct()
    {
        $this->upload_dir = base_path() . '/' . $this->upload_dir;

    }

use ResetsPasswords;

        public function getSignUpPage()
    {
        return view('auth.signup');
    }
    public function getSignInPage()
    {
        return view('auth.signin');
    }
    public function postSignUp(Request $request)
    {

        $this->validate($request, [

            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'photo' => 'sometimes|mimes:jpg,jpeg,png'


            ]);
 if ($request->hasFile('photo'))
        {
            $photo = $request->file('photo');
            $filename = $photo->getClientOriginalName();

            $destination = $this->upload_dir;

            // move the file to correct location
            if(!file_exists('user')) {
                mkdir('user', 0777, true);
            }


            $photo->move( $destination,$filename);

                                                                                                                             
           //avatar
                    if(!file_exists('user/avatar')) {
             mkdir('user/avatar', 0777, true);
            }
    $avatar = Image::make('user/' . $filename)->resize(32, null, 
    function ($constraint) {
    $constraint->aspectRatio();
    $constraint->upsize();
    })->save('user/avatar/' . $filename, 100);

        }

     
        $user = new User();
        $user->name = ucfirst($request['name']);
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']); 
 if ($request->hasFile('photo'))
{        $photo = $filename;
        $user->photo =  $photo; 
}
        $user->save();
        $user->roles()->attach(Role::where('name', 'User')->first());
        Auth::login($user);

        return redirect()->route('home');

    }
    public function postSignIn(Request $request)
    {

           $this->validate($request, [

                'email'=> 'required|email',
                'password'=> 'required|min:5',

            ]);

        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
            return redirect()->route('home');
        }
        return redirect()->route('home');
    }
    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('home');
    }

    public function getResetPassword()
    {
    	return view('auth.passwords.reset');
    }
    





}