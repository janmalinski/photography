<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\AuthorLog;


use Illuminate\Support\Facades\Session;

class AuthorsController extends Controller
{
      public function index()
    {


    	$authors = AuthorLog::orderBy('id', 'desc')->paginate(10);

    	
    	return view('admin.authors.index', compact('authors'));
    }

    public function getDeleteAuthor($id)
{
        $author = AuthorLog::find($id);
        $author->delete();

       Session::flash('flash_message', 'Author has been deleted!');
       return redirect('admin/authors');

}

}
