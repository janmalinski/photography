<?php

namespace App\Http\Controllers;



use App\Sender;

use App\ContactMessage;

use App\Events\ContactCreated;

use App\ContactLog;

use App\ContactContent;

use Illuminate\Http\Request;

use App\Http\Requests;

// Facades can write like that:

use Event; 

use Session; 

use Mail;



class ContactMessagesController extends Controller
{
    public function index()
    { $contact_message = ContactMessage::orderBy('id', 'desc')->take(1)->get();
        $contact_content = ContactContent::orderBy('id', 'desc')->take(1)->get();
    	return view('frontend.contact', compact('contact_content', 'contact_message'));
    }

    public function postContactMessage(Request $request)
    {
		
        $this->validate($request,[

                'sender' => 'required|max: 60',
                'email' => 'required|email',
                'phone'=> 'numeric',   
                'subject' => 'required|min:3',
                'message' => 'required|min:10'
            ]);




            $senderText = ucfirst($request['sender']);
            $phoneNumber = $request['phone'];
            $subjectText = $request['subject'];
            $messageText = $request['message'];

    // looking for Sender in database

           $sender = Sender::where('name', $senderText)->first();
           if(!$sender){
               $sender = new Sender();
               $sender->name = $senderText;
               $sender->email = $request['email'];
               $sender->save();             
       }

            $contact_message = new ContactMessage();
            $contact_message->phone = $phoneNumber;
            $contact_message->subject = $subjectText;
            $contact_message->message = $messageText;
            $sender->contact_messages()->save($contact_message);




        Event::fire(new ContactCreated($sender));



         // every single key becomes variable np 'email' bedzie $email 
            $data = array(
               'sender' => $request->sender,
               'email' =>  $request->email,
                'phone'=>  $request->phone,   
                'subject' => $request->subject,
                // nie moze byc message bo laravel ma zmienna message 
                'bodyMessage' => $request->message
                 );

            Mail::send('email.contact', $data, function($message) use ($data){
                $message->from($data['email']);
                $message->to('contact@beatanykiel.com');
                $message->subject($data['subject']);

            });

            Session::flash('flash_message', 'Message sent. Thanks for getting in touch. I am aim to respond within 24 hours, however this may take longer during busy periods.');
            return redirect()->back();
}

        public function getMailCallback($sender_email, $sender_name)
    {
        $contact_log = new ContactLog();
        $contact_log->email = $sender_email;
        $contact_log->name = $sender_name;
        $contact_log->save();
      
  
        return view('email.sender_callback', ['name'=> $sender_name, 'email'=> $sender_email]);
    }

         

    public function getDeleteNewsletter($id)
{
        $contact_log = ContactLog::find($id);
        $contact_log->delete();

       Session::flash('flash_message', 'Subscriber has been deleted!');
       return redirect('/contact');

}

    public function getDeleteContactMessage($id)
    {
        // find only to find $id
        $contact_message = ContactMessage::find($id);
        $sender_deleted = false;
        if(count($contact_message->sender->contact_messages) === 1) {
            $contact_message->sender->delete();
            $sender_deleted = true;
        }

            $contact_message->delete();

            $msg = $sender_deleted ? 'Contact message and sender deleted!' : 'Quote deleted!';

        Session::flash('flash_message', $msg);
    return redirect()->route('admin.index');

          // return redirect()->view('admin.index',compact('contact_messages'));
     
    }


}
