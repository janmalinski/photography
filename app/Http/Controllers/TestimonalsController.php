<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Author; 

use App\Testimonal;

use App\AuthorLog;

use Illuminate\Support\Facades\Session;

use App\Events\TestimonalCreated;

use Illuminate\Support\Facades\Event;


use Intervention\Image\Facades\Image;

class TestimonalsController extends Controller
{



    private $upload_dir =  '/public/testimonal';



    public function __construct()
    {
        $this->upload_dir = base_path() . '/' . $this->upload_dir;

    }

    public function index()
    {
        $testimonals = Testimonal::paginate(3);
        return view('admin.testimonals.index', compact('testimonals'));
    }

    public function getTestimonals()
    {
          $testimonals = Testimonal::paginate(3);
    	return view('frontend.testimonals', compact('testimonals'));
    }

    public function  getTestimonalsForm()
    {
        $testimonals = Testimonal::orderBy('created_at', 'desc')->take(1)->get();
        // ->paginate(3);

    	return view('frontend.create-testimonal', compact('testimonals'));
    }



    public function postTestimonal(Request $request)
    {
  

        $this->validate($request, [
                'author'=>'required|max:60',
                'email'=> 'required|email',
                'testimonal'=> 'required|max:1000',
                'photo'=> 'required|mimes:jpg,jpeg,png'
            ]);



            $image = $request->file('photo');

//             // set my file name
             $filename =  $image->getClientOriginalName();

            $destination = $this->upload_dir;
            // move the file to correct location
            if(!file_exists('testimonal')) {
                mkdir('testimonal', 0777, true);
            }
            $image->move($destination, $filename);

            if(!file_exists('testimonal/avatar')) {
                mkdir('testimonal/avatar', 0777, true);
}
           Image::make('testimonal/' . $filename)->resize(64,  null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
            } )->save('testimonal/avatar/' . $filename, 100);

       
     $authorText = ucfirst($request['author']);
     $testimonalText = $request['testimonal'];
     $photo = $filename;
       

        $author = Author::where('name', $authorText)->first();

        if(!$author) {

            $author = new Author();
            $author->name = $authorText;
            $author->email = $request['email'];
            $author->photo =  $photo;
            $author->save();
        }



            $testimonal = new Testimonal();
            $testimonal->testimonal = $testimonalText;


            $author->testimonals()->save($testimonal);


if($request->ajax()){

return view('frontend.testimonal-item',compact('testimonal'));
    
}
 
    }

public function TestimonalCreated(){
  
$author = Author::orderBy('created_at', 'desc')->first();
    

Event::fire(new TestimonalCreated($author));

return redirect('/');


}


   // callback w user_notification rejestrujcy authora testimnola
    public function getMailCallback($author_email, $author_name)
    {
        $author_log = new AuthorLog();

        $author_log->name = $author_name;

        $author_log->email = $author_email;

        $author_log->save();

        return view('email.user_callback', ['author'=> $author_name, 'email'=>$author_email]);
    }


    private function removePhoto($photo)
    {
        if( ! empty($photo))
        {

            $file_path =  $this->upload_dir . '/' . $photo;

            if(file_exists($file_path)) unlink($file_path);
        }
    }



    public function getDeleteTestimonal($id)
{
        $testimonal = Testimonal::find($id);
        $author_deleted = false;

        if(count($testimonal->author->testimonals) === 1 ){
            $testimonal->author->delete();
            $this->removePhoto($testimonal->author->photo);
            $author_deleted = true;
        }

        $testimonal->delete();

        $msg = $author_deleted ? 'Testimonal and author deleted' : 'Testimonal has been deleted';

       Session::flash('flash_message', $msg);
       return redirect('admin/testimonals');

}


}
