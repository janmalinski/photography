<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;

use App\Comment;

use App\Category;

use App\Tag;

use App\Testimonal;

use Auth;

use Session;

use Mail;

use Carbon;


class BlogController extends Controller
{





    public function autocomplete(Request $request)
    {

 

      if($request->ajax())
      {
        

            return Post::select(['id','title as value'])->where(function($query) use ($request){
                    if(($term = $request->get("term")))
                    {

                        $keywords = '%' .$term . '%';
                        $query->where("title", 'LIKE', $keywords);
                      
                    }
        })

        ->orderBy('title', 'asc')
        ->take(5)
        ->get();

      }
    }


    public function getArchive()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate(5);
        $comments = Comment::all();
        return view('frontend.blog.index', compact('posts', 'comments'));
    }

// public function getSingle($post_slug)
// {
//     $post = Post::where('is_active', 1)->orderBy('id', 'desc')->where('slug', $post)->first();

//         foreach($image = $post->post_images as $Fimage)
//     {
// $Fimage->orderBy('id', 'desc')->latest();
//     }


//     $categories = Category::all();

//   $tags = Tag::all();

//   $testimonals = Testimonal::orderBy('id', 'desc')->take(2)->get();

//     return view('frontend.blog.single', compact('post', 'Fimage', 'categories', 'tags', 'testimonals', 'post'));
// }





   public function getSingle($slug)
   {

      $post = Post::where('slug', '=', $slug)->first();

    // $comments =  Comment::forPost($post)->get()->threaded();


      $comments = $post->comments()->with('owner')->get()->threaded();

 
  

  // wywołane w Comment.php i zrobione w CommentCollection.php
// zmniejsza ilosć sql queries
    // tu Comment model z relacja do ownera
    // $post->load('comments.owner');

    // $comments = $post->comments->groupBy('parent_id');
    
    //  $comments['root'] = $comments[''];


    //  unset($comments['']);


    foreach($image = $post->post_images as $Fimage)
    {
$Fimage->orderBy('id', 'desc')->latest();
    }


    $categories = Category::all();

  $tags = Tag::all();

  $testimonals = Testimonal::orderBy('id', 'desc')->take(2)->get();



    return view('frontend.blog.single', compact('post', 'Fimage', 'categories', 'tags', 'testimonals', 'comments'));

   }


      public function createComment(Request $request)
    {

  $this->validate($request,[

    'body'=>'required|max:500'

    ]);

      $user =  Auth::user();
     

        $data = [

     
        'user_id'=>$user->id,
       'post_id'=>$request->post_id,
        'body'=>$request->body,
        'parent_id' =>$request->parent_id

        ];

     $comment = Comment::create($data);


      Mail::send('email.admin_comment_notification',compact('comment'), function($message)use ($comment){
    $message ->from($comment->owner->email,$comment->owner->name);
    $message->to('contact@beatanykiel.com');
    $message->subject('New comment to approve/unapprove');
      });

     

    Session::flash('flash_message', 'Your comment has been submited and is waiting moderation!');

    return redirect()->back();

    }
}

