<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\User;

use App\Role;

use App\ContactMessage;

class AdminController extends Controller{

public function index()
    {

    
    	$contact_messages = ContactMessage::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.index',compact('contact_messages'));
    }


public function getUsers()
    {

    	$users = User::all();
    	
        return view('admin.users.index',compact('users'));
    }

    public function postAdminAssignRoles(Request $request)
    {
        $user = User::where('email', $request['email'])->first();
        $user->roles()->detach();
        if ($request['role_user']) {
            $user->roles()->attach(Role::where('name', 'User')->first());
        }
        if ($request['role_client']) {
            $user->roles()->attach(Role::where('name', 'Client')->first());
        }
        if ($request['role_admin']) {
            $user->roles()->attach(Role::where('name', 'Admin')->first());
        }
        return redirect()->back();
    }
    


}