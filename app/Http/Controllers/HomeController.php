<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\Http\Requests\HomeRequest;

use Illuminate\Http\Request;

use App\Home;
 
use App\HomeSlideshow;

use Illuminate\Support\Facades\Session;





class HomeController extends Controller{

public function getHome()
    {
    

        $slides = HomeSlideshow::orderBy('order', 'asc')->get();
        $home_content = Home::orderBy('id', 'desc')->take(1)->get();
		return view('frontend.home', compact('slides', 'home_content'));
    
    }


    public function index()
    {
    	$home_content = Home::orderBy('id', 'desc')->take(1)->get();
    	return view('admin.home.index',compact('home_content'));
    }


    	public function create()
	{
 		
		return view('admin.home.create');
	}

  	public function edit($id)
  	{
  		$content = Home::find($id);
  		return view('admin.home.edit', compact('content'));
  	}

	public function store(HomeRequest $request)
	{
		Home::create($request->all());
		Session::flash('flash_message', 'Home Text Content has been created!');
		return redirect('admin/home');
	}


  		public function update($id, HomeRequest $request)
  	{
  		$content = Home::find($id);
  		$content->update($request->all());
  		Session::flash('flash_message', 'Home Text Content was updated!');
  		return redirect('admin/home');
  	}



  		public function getDeleteHomeContent($id)
  		{
			$content = Home::find($id);
			$content->delete();
			Session::flash('flash_message', 'Home Text Content was deleted!');
			return redirect('admin/home');
  		}
		 
}