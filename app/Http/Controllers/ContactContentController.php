<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\ContactContentRequest;

use App\ContactContent;

use Illuminate\Support\Facades\Session;

use Intervention\Image\Facades\Image;

class ContactContentController extends Controller
{


	private $upload_dir =  '/public/contactcontent';

	public function __construct()
	{
		$this->upload_dir = base_path() . '/' . $this->upload_dir;

	}


   

    public function index()
    {
    	$contact_content = ContactContent::orderBy('id', 'desc')->take(1)->get();
    	return view('admin.contact.index',compact('contact_content'));
    	
    }

    public function create()
    {
    return view('admin.contact.create');
    }


	public function edit($id)
	{
		$content  = ContactContent::find($id);

		return view('admin.contact.edit', compact('content'));
	}


	public function getRequest(Request $request)
	{
		$data = $request->all();

		if($request->hasFile('photo')){

			$photo = $request->file('photo');
			$file_name = $photo->getClientOriginalName();
			$destination = $this->upload_dir;
			$photo->move($destination, $file_name);

				if(!file_exists('contactcontent/thumbs')) {
		 		mkdir('contactcontent/thumbs', 0777, true);
		 	}

		 	$thumb = Image::make('contactcontent/' . $file_name)->resize(240, 160)->save('contactcontent/thumbs/' .$file_name, 100);

		 			// mobile_size
		 	 	if(!file_exists('contactcontent/480')) {
		 		mkdir('contactcontent/480', 0777, true);
		 	}
		 	$mobile_size = Image::make('contactcontent/' . $file_name)->resize(480,  null, function ($constraint) {
		    $constraint->aspectRatio();
		    $constraint->upsize();
			} )->save('contactcontent/480/' . $file_name, 100);

		// tablet_size
		 	 	if(!file_exists('contactcontent/1024')) {
		 		mkdir('contactcontent/1024', 0777, true);
		 	}
		 	$tablet_size = Image::make('contactcontent/' . $file_name)->resize(1024,  null, function ($constraint) {
		    $constraint->aspectRatio();
		    $constraint->upsize();
			})->save('contactcontent/1024/' . $file_name, 100);

		// desktop_size
		 	 	if(!file_exists('contactcontent/1140')) {
		 		mkdir('contactcontent/1140', 0777, true);
		 	}

		 	$desktop_size = Image::make('contactcontent/' . $file_name)->resize(1140,  null, function ($constraint) {
		    $constraint->aspectRatio();
		    $constraint->upsize();
			})->save('contactcontent/1140/' . $file_name, 100);

		 		$data['photo'] = $file_name;
		}
				return $data;

	}


	public function store(ContactContentRequest $request)
	{
		   $data =  $this->getRequest($request);

		   ContactContent::create($data);

		   Session::flash('flash_message', 'Contact Content has been saved!');

			return redirect('admin/contact');
	}


	


		public function update($id, ContactContentRequest $request)
	{

		
			 $content = ContactContent::find($id);
			 $oldPhoto =  $content->photo;
			 $data = $this->getRequest($request);
			 
			 $content->update($data);

			 if($oldPhoto !==  $content->photo) {

			 	$this->removePhoto($oldPhoto);

			 }


			Session::flash('flash_message', 'Contact Content has been updated!');

			return redirect('admin/contact');
	}


	private function removePhoto($photo)
	{
		if( !empty($photo))
		{

			$file_path =  $this->upload_dir . '/' . $photo;

			if(file_exists($file_path)) unlink($file_path);


			$thumb_path =  $this->upload_dir . '/thumbs/' . $photo;

			if(file_exists($thumb_path)) unlink($thumb_path);

			$mobile_size_path =  $this->upload_dir . '/480/' . $photo;

			if(file_exists($mobile_size_path)) unlink($mobile_size_path);


			$tablet_size_path =  $this->upload_dir . '/1024/' . $photo;

			if(file_exists($tablet_size_path)) unlink($tablet_size_path);


			$desktop_size_path =  $this->upload_dir . '/1140/' . $photo;

			if(file_exists($desktop_size_path)) unlink($desktop_size_path);

		}
	}
	

	public function getDeleteContactContent($id)
	{
		$content = ContactContent::find($id);
		$content->delete();
		$this->removePhoto($content->photo);
		Session::flash('flash_message', 'Contact Content has been deleted!');
		return redirect('admin/contact');
	}








}
