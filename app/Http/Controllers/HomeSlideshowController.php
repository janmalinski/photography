<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\HomeSlideshowRequest;

use App\HomeSlideshow;

use Intervention\Image\Facades\Image;

use Illuminate\Support\Facades\Session;

class HomeSlideshowController extends Controller
{

	private $limit = 2;

	private $upload_dir =  '/public/homeslideshow';



	public function __construct()
	{
		$this->upload_dir = base_path() . '/' . $this->upload_dir;

	}



	public function index()
	{


	$slides = HomeSlideshow::orderBy('order', 'desc')->paginate($this->limit);
		// instead of['contacts'=>$contacts]
    return view('admin.home-slideshow.index', compact('slides'));
	}



	public function create()
	{
 		
		return view('admin.home-slideshow.create');
	}



	public function edit($id)
	{
		$slide = HomeSlideshow::find($id);

		return view('admin.home-slideshow.edit', compact('slide'));
	}


public function getRequest(Request $request)
	{
		$data = $request->all();

		if($request->hasFile('photo')){

			$photo = $request->file('photo');
			$file_name = $photo->getClientOriginalName();
			$destination = $this->upload_dir;
			$photo->move($destination, $file_name);

				if(!file_exists('homeslideshow/thumbs')) {
		 		mkdir('homeslideshow/thumbs', 0777, true);
		 	}

		 	$thumb = Image::make('homeslideshow/' . $file_name)->resize(240, 160)->save('homeslideshow/thumbs/' .$file_name, 100);

		 			// mobile_size
		 	 	if(!file_exists('homeslideshow/480')) {
		 		mkdir('homeslideshow/480', 0777, true);
		 	}
		 	$mobile_size = Image::make('homeslideshow/' . $file_name)->resize(480,  null, function ($constraint) {
		    $constraint->aspectRatio();
		    $constraint->upsize();
			} )->save('homeslideshow/480/' . $file_name, 100);

		// tablet_size
		 	 	if(!file_exists('homeslideshow/1024')) {
		 		mkdir('homeslideshow/1024', 0777, true);
		 	}
		 	$tablet_size = Image::make('homeslideshow/' . $file_name)->resize(1024,  null, function ($constraint) {
		    $constraint->aspectRatio();
		    $constraint->upsize();
			})->save('homeslideshow/1024/' . $file_name, 100);

		// desktop_size
		 	 	if(!file_exists('homeslideshow/1170')) {
		 		mkdir('homeslideshow/1170', 0777, true);
		 	}

		 	$desktop_size = Image::make('homeslideshow/' . $file_name)->resize(1170,  null, function ($constraint) {
		    $constraint->aspectRatio();
		    $constraint->upsize();
			})->save('homeslideshow/1170/' . $file_name, 100);

		 		$data['photo'] = $file_name;
		}
				return $data;

	}

	
	public function store(HomeSlideshowRequest $request)
	{
		   $data =  $this->getRequest($request);

			HomeSlideshow::create($data);

		   Session::flash('flash_message', 'Slide has been saved!');

			return redirect('admin/home-slideshow');
	}


	


		public function update($id, HomeSlideshowRequest $request)
	{

 			$slide = HomeSlideshow::find($id);
			$oldPhoto = $slide->photo;
			$data = $this->getRequest($request);
			$slide->update($data);
			if($oldPhoto !== $slide->photo) {

			 	$this->removePhoto($oldPhoto);

			 }


  
			Session::flash('flash_message', 'Slide has been updated!');

			return redirect('admin/home-slideshow');
	}


	private function removePhoto($photo)
	{
		if( ! empty($photo))
		{

			$file_path =  $this->upload_dir . '/' . $photo;

			if(file_exists($file_path)) unlink($file_path);


			$thumb_path =  $this->upload_dir . '/thumbs/' . $photo;

			if(file_exists($thumb_path)) unlink($thumb_path);


			$mobile_size_path =  $this->upload_dir . '/480/' . $photo;

			if(file_exists($mobile_size_path)) unlink($mobile_size_path);


			$tablet_size_path =  $this->upload_dir . '/1024/' . $photo;

			if(file_exists($tablet_size_path)) unlink($tablet_size_path);


			$desktop_size_path =  $this->upload_dir . '/1170/' . $photo;

			if(file_exists($desktop_size_path)) unlink($desktop_size_path);

		}
	}


	public function getDeleteSlideshowContent($id)
	{
		$content = HomeSlideshow::find($id);
		
			$content->delete();

 			$this->removePhoto($content->photo);
		
	Session::flash('flash_message', 'Slide deleted!');
		
	return redirect('admin/home-slideshow');
			
		}


}
