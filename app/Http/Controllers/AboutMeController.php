<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\About;

use Illuminate\Support\Facades\Session;


class AboutMeController extends Controller
{
    public function getAboutMe()
    {
  
    	return view('frontend.about');
    }

    public function index()
    {
    	$about_content = About::all();
    	return view('admin.about.index', compact('about_content'));
    }
}
