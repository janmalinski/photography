<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use App\Comment;

use App\Post;

// use App\CommentCollection;

use Session;

class PostCommentsAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        $comments = Comment::orderby('created_at', 'desc')->get();

      return view('admin.comments.index',compact('comments'));
    }



    public function show($id)
    {
        $post = Post::findOrFail($id);
        $comments = $post->comments;

        return view('admin.comments.show', compact('comments'));
    }


    public function update(Request $request, $id)
    {
        Comment::findOrFail($id)->update($request->all());
        Session::flash('flash_message', 'Your comment status has been updated!');
        return redirect('admin/comments');

        
    }


    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->replies()->delete();
        $comment->delete();
        Session::flash('flash_message', 'Your comment and related replies have been deleted!');
        return redirect()->back();

    }
}
