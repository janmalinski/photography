<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::controller('books', 'JSONtestController');


// Route::post('/getmsg','ContactMessagesController@postContactMessage');


Route::get('/testimonal-created',[
    'uses'=>'TestimonalsController@TestimonalCreated',
    'as'=>'testimonal.created'

  ]);

Route::get('/signup', [
        'uses' => 'AuthController@getSignUpPage',
        'as' => 'signup'
    ]);
    Route::post('/signup', [
        'uses' => 'AuthController@postSignUp',
        'as' => 'signup'
    ]);
    Route::get('/login', [
        'uses' => 'AuthController@getSignInPage',
        'as' => 'login'
    ]);
    Route::post('/login', [
        'uses' => 'AuthController@postSignIn',
        'as' => 'login'
    ]);
    
    Route::get('/logout', [
        'uses' => 'AuthController@getLogout',
        'as' => 'logout'
    ]);

// Route::get('/password/reset',[
//    'uses' => 'AuthController@getResetPassword',
//     'as' => 'resetpassword'

//   ]
   
// );

// Password Reset Routes
Route::get('password/reset/{token?}', 'PasswordController@showResetForm');
Route::post('password/email', 'PasswordController@sendResetLinkEmail');
route::post('password/reset', 'PasswordController@reset');



 Route::get('/', [
    'uses'=> 'HomeController@getHome',
    'as' => 'home'
    ]);


 Route::get('/galleries', [
    'uses'=> 'GalleryController@getGalleries',
    'as' => 'galleries'
    ]);


 Route::get('/galleries/{slug}', [
    'uses'=> 'GalleryController@getGallery',
    'as' => 'gallery'
    ]);


 Route::get('/sessions', [
    'uses'=> 'SessionsController@getSessions',
    'as' => 'sessions'
    ]);


 Route::get('/testimonals', [
    'uses'=> 'TestimonalsController@getTestimonals',
    'as' => 'testimonals'
    ]);

// Route::resource('testimonals', 'TestimonalsController');






Route::get('/gotemail/{author_email}/{author_name}',[
    'uses'=>'TestimonalsController@getMailCallback',
    'as'=> 'mail_callback'
  ]);

 Route::get('/about-me', [
    'uses'=> 'AboutMeController@getAboutMe',
    'as' => 'about'
    ]);

 Route::get('/contact', [
    'uses'=> 'ContactMessagesController@index',
    'as' => 'contact'
    ]);


 Route::post('/contact', [
    'uses'=> 'ContactMessagesController@postContactMessage',
    'as' => 'contact.store'
    ]); 

 Route::get('/got-sender-email/{sender_email}/{sender_name}',[
    'uses'=>'ContactMessagesController@getMailCallback',
    'as'=> 'sender_mail_callback'
  ]);


Route::get('/photography-blog',[
    'uses' => 'BlogController@getArchive',
    'as'=>'blog.index'
  ]);


Route::get('photography-blog/{slug}',
  [
  'uses'=> 'BlogController@getSingle',
  'as'=>'blog.single'

  ])->where('slug', '[\w\d\-\_]+');


Route::group([
       
          'middleware' => 'roles',
      'roles'=>['Client']
        ]
        , function () {



Route::get('/create-testimonal',[
    'uses'=> 'TestimonalsController@getTestimonalsForm',
    'as' =>  'testimonal.create'
  ]);

Route::post('/create-testimonal',[
    'uses'=> 'TestimonalsController@postTestimonal',
    'as' =>  'testimonal.store'
  ]);



});

Route::get('posts/autocomplete',
 ['uses'=>'BlogController@autocomplete', 
 'as'=>'blog.autocomplete']);


Route::group([
       
          'middleware' => 'roles',
      'roles'=>['Admin']
        ]
        , function () {


Route::get('newsletter/{id}/delete',
 [
  'uses' => 'ContactMessagesController@getDeleteNewsletter',
  'as'=> 'newsletter_delete']);



});




Route::post('/comment',[
'uses'=>'BlogController@createComment',
'as'=>'createComment',
  'middleware' => 'roles',
      'roles'=>['User','Admin']]);




   
  Route::group([
			   'prefix'=> '/admin',
          'middleware' => 'roles',
      'roles'=>['Admin']
				]
        , function () {


Route::get('/', [
		'uses'=>'AdminController@index',
		'as'=> 'admin.index'
	]);

Route::get('users',[
      'uses'=> 'AdminController@getUsers',
      'as'=> 'admin.users'
  ]);


Route::post('users',[
      'uses'=> 'AdminController@postAdminAssignRoles',
      'as'=> 'admin.assign'
  ]);

Route::resource('home', 'HomeController');


Route::get('home/{id}/delete',[
        'uses' => 'HomeController@getDeleteHomeContent',
        'as'=> 'admin.home.delete'
          ]);

Route::resource('home-slideshow', 'HomeSlideshowController');

Route::get('home-slideshow/{id}/delete',[
        'uses' => 'HomeSlideshowController@getDeleteSlideshowContent',
        'as'=> 'admin.home-slideshow.delete'
          ]);

Route::resource('gallery', 'GalleryController');

Route::get('gallery/{id}/delete',
 [
  'uses' => 'GalleryController@getDeleteGalleryContent',
  'as'=> 'admin.gallery.delete']);

Route::post('image/do-upload', 'GalleryController@doImageUpload');


Route::get('images/{id}/delete',
 [
  'uses' => 'GalleryController@getDeleteImagesContent',
  'as'=> 'admin.images.delete']);

Route::get('gallery/delete/{id}', 'GalleryController@getDeleteGallery');

Route::resource('contact', 'ContactContentController');

Route::get('contact/{id}/delete',
 [
  'uses' => 'ContactContentController@getDeleteContactContent',
  'as'=> 'admin.contact.delete']);

Route::get('contact-message/{id}/delete',
 [
  'uses' => 'ContactMessagesController@getDeleteContactMessage',
  'as'=> 'admin.contact-message.delete']);

Route::resource('sessions', 'SessionsController');

Route::get('sessions/{id}/delete',
 [
  'uses' => 'SessionsController@getDeleteVideoSession',
  'as'=> 'admin.session.delete']);

Route::resource('about-me', 'AboutMeController');


Route::resource('testimonals', 'TestimonalsController');

Route::get('testimonal/{id}/delete',
 [
  'uses' => 'TestimonalsController@getDeleteTestimonal',
  'as'=> 'admin.testimonal.delete']);


Route::resource('newsletter', 'NewsletterController');

Route::get('newsletter/{id}/delete',
 [
  'uses' => 'NewsletterController@getDeleteNewsletter',
  'as'=> 'admin.newsletter.delete']);

Route::resource('authors', 'AuthorsController');

Route::get('authors/{id}/delete',
 [
  'uses' => 'AuthorsController@getDeleteAuthor',
  'as'=> 'admin.authors.delete']);


// Route::resource('blog', 'BlogController');


Route::resource('posts', 'PostController');

Route::resource('categories', 'CategoryController', ['except'=> ['create']]);


Route::post('post-image/do-upload', 'PostController@doPostImageUpload');


Route::get('posts/{id}/delete',
 [
  'uses' => 'PostController@getDeletePost',
  'as'=> 'admin.posts.delete']);


Route::resource('tags', 'TagController', ['except' => ['create']]);

Route::get('tags/{id}/delete',
 [
  'uses' => 'TagController@getDeleteTag',
  'as'=> 'admin.tags.delete']);

Route::resource('comments', 'PostCommentsAdminController');



});

