<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{

	protected $table = 'authors';

	protected $fillable = ['name', 'email', 'photo'];

    public function testimonals()
    {
    	return $this->hasMany('App\Testimonal');
    }
}
