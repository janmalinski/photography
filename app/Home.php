<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{

	//custom name of table
    protected $table="home";

    protected $fillable = array(
	'introduction_title', 
	'introduction', 
	'advantages_title',
	'advantages',
	'how_i_work_title',
	'how_i_work'
	);
}
