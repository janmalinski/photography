<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Testimonal extends Model
{

	protected $table = 'testimonals';

	protected $fillable = ['author','email','testimonal','photo'];

    public function author()
    {
    	return $this->belongsTo('App\Author');
    }


}
