<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $fillable = [
    'user_id',
    'title',
    'slug',
    'image',
    'body'
    ];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function tags()
    {
    	return $this->belongsToMany('App\Tag');
    }
    // public function comments()
    // {
    //     return $this->hasMany('App\Comment');
    // }

        public function post_images()
    {
        return $this->hasMany('App\PostImage');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

// public function Comments()
// {
//     return $this->hasMany('App\Comment');
// }
    public function comments()

{

        return $this->hasMany('App\Comment');

}




}
