<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{

protected $table = 'post_images';

	

    protected $fillable = [
    'post_id',
    'file_name',
    'file_path',
    'thumb_photo_path',
     'xsmall_photo_path',
    'small_photo_path',
    'medium_photo_path',
    'large_photo_path',
   'xlarge_photo_path'
    ];


   public function post()
{
	return $this->belongsTo('App\Post');
}
}
