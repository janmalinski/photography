<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

protected $table = 'images';

	

    protected $fillable = [
    'gallery_id',
    'file_path',
    'file_name',
    'small_photo_path',
   'thumb_photo_path' 
    ];


public function gallery()
{
	return $this->belongsTo('App\Gallery');
}


}
