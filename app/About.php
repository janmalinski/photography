<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
 protected $table = 'about_content';

	protected $fillable = ['title','description','photo'];
}
