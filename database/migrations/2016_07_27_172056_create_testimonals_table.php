<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('testimonals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->text('testimonal');
            $table->integer('author_id');

 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
      Schema::drop('testimonals');
    }
}
