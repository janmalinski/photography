<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id');
            $table->string('file_name');
            $table->string('file_path');
            $table->string('thumb_photo_path');
            $table->string('xsmall_path');
            $table->string('small_path');
            $table->string('medium_path');
            $table->string('large_path');
            $table->string('xlarge_path');
            $table->string('small_photo_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_images');
    }
}
