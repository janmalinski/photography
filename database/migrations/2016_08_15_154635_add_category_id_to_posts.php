<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIdToPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            // unisgned = only postive numbers
            $table->integer('category_id')->nullable()->unsigned();


            //$table->integer('category_id') 

            // the sam like:


            //$table->foreign('category_id')->references('id')->on('categories');


            // last has worst support so don't use it!
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            //
        });
    }
}
