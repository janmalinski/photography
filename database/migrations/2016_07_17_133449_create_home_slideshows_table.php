<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeSlideshowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return voidcomposer update
     
     */
    public function up()
    {
        Schema::create('home_slideshow', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->integer('order')->unique();
            $table->string('photo');
            $table->timestamps();
    });
}
    /**
     * Reverse the migrations.
     *php artisan 
     * @return void
     */
    public function down()
    {
        Schema::drop('home_slideshow');
    }
}
