<?php

use Illuminate\Database\Seeder;

use App\User;

use App\Role;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$role_admin = Role::where('name', 'Admin')->first();
    	$role_client = Role::where('name', 'Client')->first();
    	$role_user = Role::where('name', 'User')->first();


        $admin = new User();
        $admin->name = 'Beata';
        $admin->email = 'beata.nykiel@gmail.com';
        $admin->password = bcrypt('Blanowska');
        $admin->save();
        $admin->roles()->attach($role_admin);

   		$client = new User();
        $client->name = 'Bartosz';
        $client->email = 'bartosz@gmail.com';
        $client->password = bcrypt('Bartosz');
        $client->save();
        $client->roles()->attach($role_client);


        $user = new User();
        $user->name = 'Jan';
        $user->email = 'jan.malinski81@gmail.com';
        $user->password = bcrypt('London2016');
        $user->save();
        $user->roles()->attach($role_user);

    }
}
