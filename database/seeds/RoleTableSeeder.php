<?php

use Illuminate\Database\Seeder;

use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role_admin = new Role();
        $role_admin->name = 'Admin';
        $role_admin->description = 'An  Admin';
        $role_admin->save();

        $role_client = new Role();
        $role_client->name = 'Client';
        $role_client->description = 'A Client';
        $role_client->save();


        $role_user = new Role();
        $role_user->name = 'User';
        $role_user->description = 'A normal User';
        $role_user->save();

    }
}
