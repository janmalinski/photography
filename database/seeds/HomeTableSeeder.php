<?php

use Illuminate\Database\Seeder;

use App\Home;

class HomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $home_content = new Home();
       $home_content->introduction_title = 'm dolor amet leberkas kielbasa t';
       $home_content->introduction = 'Bacon ipsum dolor amet tenderloin turducken ribeye boudin venison, prosciutto ham hock bresaola bacon flank pork loin. T-bone pork loin leberkas, brisket picanha kevin salami venison doner shank beef porchetta';
       $home_content->advantages_title = 'Bacon ipsum dolor';
       $home_content->advantages = 'Bacon ipsum dolor amet tenderloin turducken ribeye boudin venison, prosciutto ham hock bresaola bacon flank pork loin. T-bone pork loin leberkas, briske';
 		$home_content->how_i_work_title = 'm dolor amet leberkas kielbasa';
		$home_content->how_i_work = ' amet tenderloin turducken ribeye boudin venison, prosciutto ham hock bresaola bacon flank pork loin. T-bone pork loin leb';
       $home_content->save();
    }
}
