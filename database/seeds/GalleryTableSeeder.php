<?php

use Illuminate\Database\Seeder;
use App\Gallery;


class GalleryTableSeeder extends Seeder
{

    public function run(){

        Gallery::create([
        	'title' => 'Title First One',
        	'description' => 'Landjaeger ham beef tail. Andouille brisket spare ribs doner, ball tip ham hock chicken kielbasa porchetta tenderloin chuck. Shoulder tongue corned beef, porchetta pork chop chicken strip steak shankle drumstick. Tri-tip short ribs landjaeger, swine venison kevin sausage porchetta jerky turducken biltong ribeye andouille turkey rump. Chuck strip steak shankle, prosciutto cow brisket salami sirloin hamburger landjaeger t-bone. Turkey venison landjaeger swine strip steak pancetta tail kevin ribeye porchetta flank leberkas jowl drumstick ball tip. Filet mignon rump biltong, andouille pork pork chop kielbasa ribeye alcatra.',
        	'slug' => 'title-first-one',
        	'photo' => '3.jpg'
        ]);

              Gallery::create([
        	'title' => 'Title Second Two',
        	'description' => 'Landjaeger ham beef tail. Andouille brisket spare ribs doner, ball tip ham hock chicken kielbasa porchetta tenderloin chuck. Shoulder tongue corned beef, porchetta pork chop chicken strip steak shankle drumstick. Tri-tip short ribs landjaeger, swine venison kevin sausage porchetta jerky turducken biltong ribeye andouille turkey rump. Chuck strip steak shankle, prosciutto cow brisket salami sirloin hamburger landjaeger t-bone. Turkey venison landjaeger swine strip steak pancetta tail kevin ribeye porchetta flank leberkas jowl drumstick ball tip. Filet mignon rump biltong, andouille pork pork chop kielbasa ribeye alcatra.',
        	'slug' => 'title-second-two',
        	'photo' => '3.jpg'
        ]);
    }
}
